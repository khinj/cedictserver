﻿CedictServer
============

[English description is here.](../readme.md)

A CedictServer egy segédeszköz Windows 10-en kínai nyelvtanuláshoz, amely a CEDICT angol-kínai szótárat használja.  
Töltse le ezt a szótárat a [CEDICT weboldaláról](http://www.mdbg.net/chindict/chindict.php?page=cedict) (cedict_1_0_ts_utf-8_mdbg.zip vagy cedict_1_0_ts_utf-8_mdbg.txt.gz), csomagolja ki, és a Beállítások panelen, a "CEDICT szótár fájl (pl. cedict_ts.u8) helye" alatti mezőben adja meg ennek helyét.
Használhat saját szótárat is, ha abban a sorok formátuma:  
    kínai szó÷pinyin÷fordítás  
, ennek helyét is adja meg.  
A panelek között az egér kattintáson kívül a Ctrl + PageUp/PageDown billentyűkkel is mozoghat. A betűméret beállítható szöveges mezőkben a Ctrl + egér görgővel. Lehetőség van a szövegmezők betűtípusának, betűszínének és háttérszínének beállítására is.

Japán nyelv is használható. Ehhez töltse le a JMdict_e.gz fájlt a [JMDict Project](https://www.edrdg.org/jmdict/j_jmdict.html) weboldaláról (a "the current version with only the English translations" sorban), tömörítse ki, és írja be a JMDict_e fájl helyét a Beállítások panelen. Ennek használata pár perccel megnöveli a CedictServer indítását.


**Funkciók:**
* Kereső:  
A legördülő lista szerinti függvényeket használva (pl. vChToPin: kínai szöveg -> pinyin szöveg) végezhet keresést. Ha a "Saját szótár használat" be van jelölve, és az tartalmazza a keresett szöveget, akkor a CEDICT szótában nem keres. A FvChToPin használatakor a CEDICT szótában levő nagy kezdőbetűs pinyin szöveg (tulajdonnév) csak akkor jelenik meg, ha a "Nagybetűs pinyin" be van jelölve.

* Beállítások  
Az első használat kezdetekor meg kell adnia a CEDICT szótár fájl és a saját szótár fájl helyét (mintaként a cedict_ts_teszt.u8" és "localLista.txt" van megadva).
A panel alsó felének használatával lehetőség van egy **adott \*.srt feliratfájlba a kínai szövegek alá beírni a pinyin szövegeket**. A forrásfájl lehet bármi más szöveges fájl is.

* HTML Request  
A CedictServer futása alatt lehetőség van arra, hogy más rogramok használják a Kereső panel legördülő listája szerinti függvényeket az 50000-es porton keresztül. Az ebben a projektben levő _CS_TCP_IP.xls egy ilyen alkalmazás. A web böngészőben ezt is kipróbálhatja:  
    http://localhost:50000/FvChToPin|%E4%BD%A0%E5%A5%BD  
(A válasz: nǐhǎo.)

* Pipe  
A CedictServer futása alatt lehetőség van arra, hogy más rogramok használják a Kereső panel legördülő listája szerinti függvényeket pipe-on (szoftveres kommunikációs csatornán) keresztül. Ilyen az internetről letölthető [SaVLC médialejátszó]((https://bitbucket.org/khinj/savlc/).

A program innen tölthető le: [CedictServer.zip](https://1drv.ms/f/s!AhEINfn0bmWKgQavfQq5bVxJpoRv). Kicsomagolás után futtassa a CedictServer\release\CedictServer.exe programot.
		
![Screenshot_1](CS_1.jpg "Screenshot_1")    ![Screenshot_2](CS_2.jpg "Screenshot_2")    ![Screenshot_3](CS_3.jpg "Screenshot_3")


**Fejlesztőknek:**

Ezeket a "hozzávalókat" használtam a CedictServer fejlesztéséhez:

* Windows 10 operation system (Win OS)

* Qt4.8

* Qt Creator 2.5.2

* MinGW (gcc (GCC) 4.8.1)

* Git


**Licensz:**

Az általam hozzáadott rész teljesen szabadon felhasználható. A fentebb említett "hozzávalók" licenszei a nekik megfelelő webhelyeken vannak. Nincs semmi garancia.
