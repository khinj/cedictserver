#ifndef TCPIPSOCKET_H
#define TCPIPSOCKET_H

#include <QHostAddress>
#include <QString>
#include <QTcpSocket>
#include <QTime>
#include <QTimer>

static const int MaxBufferSize = 1024000;

class TcpIPSocket : public QTcpSocket
{
    Q_OBJECT

public:
    TcpIPSocket(QObject *parent = 0);
    bool sendMessage(QString message);

signals:

private slots:
    void processReadyRead();

private:
    int readDataIntoBuffer(int maxSize = MaxBufferSize);
    QString saHex(QByteArray x);
    QByteArray buffer;
    QString httpMethod;
};

#endif // TCPIPSOCKET_H
