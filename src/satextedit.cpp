﻿#include <QScrollBar>
#include <QClipboard>
#include "satextedit.h"
#include "mainwindow.h"

extern MainWindow *w;

SaTextEdit::SaTextEdit(QWidget *parent) :
    QTextEdit(parent)
{
    setAcceptRichText(false);
    QString st = "SaTextEdit {";
    st += "border: 0px solid blue; ";
    st += "color: rgba(255, 255, 0); ";
        //SaTextEdit azért kell, hogy a context menü szürke maradjon
    st += "background: rgba(0, 0 ,0, 100%); ";
    st += "}";
    //qWarning("Hello1: st :" + st.toLatin1());
    this->setAutoFillBackground(true);
    this->setStyleSheet(st);
}

void SaTextEdit::keyPressEvent(QKeyEvent *e) {
//    qWarning("e->modifiers(): %d, e->key(): %d", (int) e->modifiers(), (int) e->key());
    if (    (e->modifiers() == 33554432  && e->key() == 16777248) ||  //csak Shift
            (e->modifiers() == 67108864  && e->key() == 16777249) ||  //csak Ctrl
            (e->modifiers() == 134217728 && e->key() == 16777251) ||  //csak Alt
            (e->modifiers() == 0         && e->key() == 16777252) ||  //csak CapsLock
            (e->modifiers() == 536870912 && e->key() == 16777253) ||  //csak NumLock
            (e->modifiers() == 0         && e->key() == 16777216) ||  //csak Esc
            (e->modifiers() == 0         && e->key() == 16777250) ||  //csak Window
            (e->modifiers() == 100663296 && e->key() == 16777248) ||  //csak Ctrl+Shift
            (e->modifiers() == 167772160 && e->key() == 16777248) ||  //csak Alt+Shift
            (e->modifiers() == 201326592 && e->key() == 16777249) ) {  //csak Ctrl+Alt
    } else if (e->matches(QKeySequence::Copy)) {
        QString st = "";
        if (this->selList.size() > 0) {
            for(int i=0; i<this->selList.size(); i++) {
                st = st + toPlainText().mid(
                        this->selList[i].x(),
                        this->selList[i].y() - selList[i].x());
            }
        } else {
            st = this->textCursor().selectedText();
        }
        QClipboard *clipboard = QApplication::clipboard();
        clipboard->setText(st);
    } else if (e->modifiers() == Qt::KeypadModifier && e->key() == Qt::Key_4) {
        if (this->toPlainText() != "") {
            this->setFocus();
            if (selList.size() == 0) {
                QTextCursor cur = this->textCursor();
                selList.append(QPoint(cur.selectionStart(), cur.selectionEnd()));
            }
            int pos1 = selList.last().x();
            int pos2 = selList.last().y();
            int hossz = this->toPlainText().length();
            if (pos2-pos1 > 1) {
                selList.last().setX(pos2-1);
                selList.last().setY(pos2);
            } else if (pos2 == pos1) {
                if (pos1 > 0) {
                    selList.last().setX(pos1-1);
                    selList.last().setY(pos2);
                } else {
                    selList.last().setX(hossz-1);
                    selList.last().setY(hossz);
                }
            } else {
                if (pos2 > 1) {
                    selList.last().setX(pos2-2);
                    selList.last().setY(pos2-1);
                } else if (selList.size() == 1){
                    selList.last().setX(hossz-1);
                    selList.last().setY(hossz);
                }
            }
            int n = selList.size();
            if (n > 1 &&
                    selList[n-2].y() > selList[n-1].x()) {
                selList.removeLast();
                selList.last().setY(selList.last().y() + 1);
            }
            w->helyiValaszSelected();
        }
    } else if (e->modifiers() == Qt::KeypadModifier && e->key() == Qt::Key_5) {
        if (this->toPlainText() != "") {
            this->setFocus();
            if (selList.size() == 0) {
                QTextCursor cur = this->textCursor();
                selList.append(QPoint(cur.selectionStart(), cur.selectionEnd()));
            }
            int pos1 = selList.last().x();
            int pos2 = selList.last().y();
            int hossz = this->toPlainText().length();
            if (pos2 < hossz) {
                if (pos1 == pos2) {
                    selList.last().setX(pos1);
                    selList.last().setY(pos1+1);
                } else {
                    selList.last().setX(pos1+1);
                    selList.last().setY(pos1+2);
                }
            } else  if (selList.size() == 1) {
                selList.last().setX(0);
                selList.last().setY(1);
            }
            w->helyiValaszSelected();
        }
    } else if (e->modifiers() == Qt::KeypadModifier && e->key() == Qt::Key_6) {
        if (this->toPlainText() != "") {
            this->setFocus();
            if (selList.size() == 0) {
                QTextCursor cur = this->textCursor();
                selList.append(QPoint(cur.selectionStart(), cur.selectionEnd()));
            }
            int pos1 = selList.last().x();
            int pos2 = selList.last().y();
            if (pos2 < this->toPlainText().length()) {
                pos2++;
            }
            selList.last().setX(pos1);
            selList.last().setY(pos2);
            w->helyiValaszSelected();
        }
    } else if (e->modifiers() == Qt::KeypadModifier && e->key() == Qt::Key_8) {
        if (this->toPlainText() != "") {
            if (selList.size() != 0) {
                int pos1 = selList.last().y();
                selList.clear();
                selList.append(QPoint(pos1, pos1));
                w->helyiValaszSelected();
            }
        }
    } else if (e->modifiers() == Qt::KeypadModifier && e->key() == Qt::Key_9) {
        if (this->toPlainText() != "") {
            this->setFocus();
            if (selList.size() == 0) {
                QTextCursor cur = this->textCursor();
                selList.append(QPoint(cur.selectionStart(), cur.selectionEnd()));
            }
            int pos1 = selList.last().x();
            int pos2 = selList.last().y();
            if (pos2 < this->toPlainText().length()) {
                pos1 = pos2;
                pos2 = pos2 + 1;
                selList.append(QPoint(pos1, pos2));
            }
            w->helyiValaszSelected();
        }
    } else if (e->modifiers() == Qt::KeypadModifier && e->key() == Qt::Key_7) {
        w->ui->checkBox_5->toggle();
        w->helyiValaszSelected();
    } else if (e->modifiers() == Qt::AltModifier && e->key() == Qt::Key_Left) {
            this->selTorol();
            w->szoListVisszaUgras();
            w->kerdezTextEdit->textCursor().setPosition(
                    w->kerdezTextEdit->toPlainText().length() / 2);
    } else if (e->modifiers() == Qt::AltModifier && e->key() == Qt::Key_Right) {
            this->selTorol();
            w->szoListEloreUgras();
            w->kerdezTextEdit->textCursor().setPosition(
                    w->kerdezTextEdit->toPlainText().length() / 2);
    } else if (e->modifiers() == Qt::AltModifier && e->key() == Qt::Key_Down) {
        if (this->objectName() == "kerdezTextEdit") {
            w->ui->comboBox->setFocus();
        } else {
            QTextEdit::keyPressEvent(e);
        }
    } else {
        this->selTorol();
        QTextEdit::keyPressEvent(e);
    }
    int pos = 0;
    if (selList.size() > 0) {
        pos = selList.last().y();
    }
    if (this->objectName() == "kerdezTextEdit") {
        if (pos <= 5) {
            w->valaszTextEdit->verticalScrollBar()->setValue(
                    w->valaszTextEdit->verticalScrollBar()->minimum());
        } else if (pos >= this->toPlainText().length()-5) {
            w->valaszTextEdit->verticalScrollBar()->setValue(
                    w->valaszTextEdit->verticalScrollBar()->maximum());
        }
    }
}

void SaTextEdit::wheelEvent(QWheelEvent *e) {
    if (e->modifiers() & Qt::ControlModifier) {
        w->wheelEvent(e);
    } else {
        QTextEdit::wheelEvent(e);
    }
}

void SaTextEdit::saSetStyleSheet(QString par, QString value) {
    QString st = this->styleSheet();
    st.replace(QRegExp(par + ":[^;]*;"), par + ": " + value + ";");
    this->setStyleSheet(st);
}

void SaTextEdit::setBetuSzin(QColor col) {
    this->hide();
    this->setTextColor(col);
    this->betuSzin = col;
    this->setTextColor(col);
    this->saSetStyleSheet("color", "rgba(" +
            QString::number(col.red()) + ", " +
            QString::number(col.green()) + ", " +
            QString::number(col.blue()) + ", 100%)");
    //kijelölés mindig legyen feltűnő:
    QPalette p = this->palette();
    p.setColor(QPalette::HighlightedText, this->hatterSzin);
    p.setColor(QPalette::Highlight, this->betuSzin);
    this->setPalette(p);
    this->show();
}

void SaTextEdit::setHattertSzin(QColor col) {
    //qWarning("setHatterSzin:");
    this->hide();
    //Ez a karakterek háttérszíne:
    this->setTextBackgroundColor(col);
    this->hatterSzin = col;
    //Ez a SaTextEdit háttérszíne:
    QPalette pal = this->palette();
    pal.setColor(QPalette::Base, col);
    this->setPalette(pal);
    //Ez a StyleSheet beállítás:
    this->saSetStyleSheet("border", "0px solid blue");
    this->saSetStyleSheet("background", "rgba(" +
            QString::number(col.red()) + ", " +
            QString::number(col.green()) + ", " +
            QString::number(col.blue()) + ", 100%)");
    //kijelölés mindig legyen feltűnő:
    QPalette p = this->palette();
    p.setColor(QPalette::HighlightedText, this->hatterSzin);
    p.setColor(QPalette::Highlight, this->betuSzin);
    this->setPalette(p);
    this->show();
}

void SaTextEdit::mousePressEvent(QMouseEvent *e) {
    if (e->buttons() == Qt::LeftButton && this->objectName() == "kerdezTextEdit") {
        int pos = (this->cursorForPosition(e->pos())).position();
        if (e->modifiers() == Qt::NoModifier) {
            this->selList.clear();
            this->selList.append(QPoint(pos, pos));
        } else if (e->modifiers() == Qt::ShiftModifier) {
            if (this->selList.size() == 0) {
                this->selList.append(QPoint(pos, pos));
            } else {
                while (this->selList.size() > 0 && pos < this->selList.last().x()) {
                    this->selList.removeLast();
                }
                if (pos < this->selList.last().x()) {
                    this->selList.last().setX(pos);
                } else {
                    this->selList.last().setY(pos);
                }
            }
        } else if (e->modifiers() == Qt::ControlModifier) {
            while (this->selList.size() > 0 && pos < this->selList.last().x()) {
                this->selList.removeLast();
            }
            this->selList.append(QPoint(pos, pos));
        }
        w->helyiValaszSelected();
    } else {
        QTextEdit::mousePressEvent(e);
    }
}

void SaTextEdit::mouseMoveEvent(QMouseEvent *e) {
    if (e->buttons() == Qt::LeftButton && this->objectName() == "kerdezTextEdit") {
        int pos = (this->cursorForPosition(e->pos())).position();
        if (this->selList.size() == 0) {
            this->selList.append(QPoint(pos, pos));
        } else {
            while (this->selList.size() > 0 && pos < this->selList.last().x()) {
                this->selList.removeLast();
            }
            if (this->selList.size() == 0) {
                this->selList.append(QPoint(pos, pos));
            } else {
                this->selList.last().setY(pos);
            }
        }
        w->helyiValaszSelected();
    } else {
        QTextEdit::mousePressEvent(e);
    }
}

void SaTextEdit::selTorol() {
    int pos1 = -1;
    int pos2 = -1;
    if (this->selList.size() > 0) {
        pos1 = this->selList.last().x();
        pos2 = this->selList.last().y();
    }
    this->selList.clear();
    QList<QTextEdit::ExtraSelection> extraSelections;
    this->setExtraSelections(extraSelections);
    if (pos1 > -1) {
        QTextCursor cur = this->textCursor();
        cur.setPosition(pos1, QTextCursor::MoveAnchor);
        cur.setPosition(pos2, QTextCursor::KeepAnchor);
        this->setTextCursor(cur);
    }
}
