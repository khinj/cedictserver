#ifndef TCPIPSERVER_H
#define TCPIPSERVER_H

#include <QTcpServer>

class TcpIPSocket;

class TcpIPServer : public QTcpServer
{
    Q_OBJECT

public:
    TcpIPServer(QObject *parent = 0);

signals:
    void newConnection(TcpIPSocket *connection);

protected:
    void incomingConnection(int socketDescriptor);

};

#endif // TCPIPSERVER_H
