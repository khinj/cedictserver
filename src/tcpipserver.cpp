#include <QtNetwork>

#include "tcpipsocket.h"
#include "tcpipserver.h"
#include "windows.h"
#include "Szotar.h"
#include "mainwindow.h"

extern MainWindow *w;
extern Szotar *sz;

TcpIPServer::TcpIPServer(QObject *parent) : QTcpServer(parent)
{
    if (!listen(QHostAddress::Any, 50000)) {
        listen(QHostAddress::Any);
    }
    w->konzolOut(w->satr("TcpIP szerver: várakozás a kliens kapcsolódásra ezen a porton") +
                 ": {0}\n", serverPort(), false);
}

void TcpIPServer::incomingConnection(int socketDescriptor)
{
//    qWarning("B001");
    TcpIPSocket *connection = new TcpIPSocket(this);
    connection->setSocketDescriptor(socketDescriptor);
//    emit newConnection(connection);
}
