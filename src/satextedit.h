﻿#ifndef SATEXTEDIT_H
#define SATEXTEDIT_H

#include <QTextEdit>
#include <QKeyEvent>

class SaTextEdit : public QTextEdit
{
    Q_OBJECT
public:
    explicit SaTextEdit(QWidget *parent = 0);
    void keyPressEvent(QKeyEvent *e);
    void wheelEvent(QWheelEvent *e);
    void mousePressEvent(QMouseEvent *e);
    void mouseMoveEvent(QMouseEvent *e);
    void saSetStyleSheet(QString par, QString value);
    QColor betuSzin;
    QColor hatterSzin;
    void setBetuSzin(QColor col);
    void setHattertSzin(QColor col);
    QList<QPoint> selList;
    void selTorol();

signals:
    
public slots:
    
};

#endif // SATEXTEDIT_H
