﻿#pragma once
#include "MyList.h"
#include "MyList.cpp"   //ez kell a template működéséhez!!!
#include <QHash>

class Szotar
{
public:
	Szotar();
	~Szotar();
    QString ValaszFv(QString kerdez);
    QString ValaszFv2(QString kerdez);
    QString Nyers1Alap(QString kerdez, bool isPin15, bool csakCedictbenKeres);
    QString Fv1Nyers(QString kerdez);
    QString Fv1NyersB(QString kerdez);
    QString NyersMAlap(QString kerdez0, bool isPin15, bool csakCedictbenKeres);
    QString FvMNyers(QString kerdez);
    QString FvMNyersB(QString kerdez);
    QString FvChToEn(QString kerdez);
    QString FvChToEnNyers(QString kerdez);
    QString FvChToSt(QString kerdez);
    QString FvChToTr(QString kerdez);
    QString FvJpToLat(QString kerdez);
    QString FvJpToEnNyers(QString kerdez);
    QString FvJpToEn(QString kerdez);

    QString szotKeres(QString kerdez, bool csakCedictbenKeres);
    QString toneEsNagybetusKorrekcio(QString ch, QString pin15_0);
    QString getTone(QString pin15);
    QString setTone(QString pin15, QString tone);
    QString nagybetusTone(QString pin15);

    MyList<long> listL;     //CEDICT szótár listája
    QString nyersetTagol(QString kerdez);
	QStringList nyersL;
    int talalHossz;
    bool szotarBeolvas();
    bool szotarBeolvas(QString fileName, int type);

    MyList<long> jpListL;  //JMdict szótár listái
    QStringList jpNyersL;   //SZEPA1 + kiejtés + SZEPA2 + jelentés + SZEPAENT_SEC + ent_seq + SZEPA3 + SZEPA1 + kiejtés + SZEPA2 + jelentés + SZEPAENT_SEC + ent_seq  + SZEPA3

    MyList<QString> localL; //saját szótár listája (local: saját)
    bool localSzotarBeolvas();
    bool localSzotarBeolvas(QString fajlNev0, bool kellAdmin);
    QString nyersetTagolLocal(QString kerdez);
    MyList<QString> pinPin15L;
    QHash<QString, QString> pin15PinH;
    void pinBeolvas();
    QString FvChToPin(QString kerdez);
    QString PinToPin15(QString x0);
    QString Pin15ToPin(QString x0);
    void Pin15LKiiras(QList<QStringList> *pin15L, QString elsoSor);
    QString eredetiCedictSzotarFajlNev;
    QString eredetiJMdictSzotarFajlNev;
    QString eredetiLocalSzotarFajlNev;
    void srtKonverter(QString forrasFajlNev, QString celFajlNev);
    int szoListIdx;
    QStringList szoList;
    QHash<QString, QString> entityL;
    MyList<QString> syllabaryL;
    QString rebToLat(QString);
    void jpSzotBerak(QString keb, QString reb, QString sense);

};
