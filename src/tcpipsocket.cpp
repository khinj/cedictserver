#include "tcpipsocket.h"

#include <QtNetwork>
#include <QTextCodec>
#include "Szotar.h"
#include "mainwindow.h"

extern MainWindow *w;
extern Szotar *sz;

TcpIPSocket::TcpIPSocket(QObject *parent) : QTcpSocket(parent)
{
    QObject::connect(this, SIGNAL(readyRead()), this, SLOT(processReadyRead()));
    QObject::connect(this, SIGNAL(disconnected()), this, SLOT(deleteLater()));
}

void TcpIPSocket::processReadyRead()
{
    int a = readDataIntoBuffer();
    QString st = QString::fromAscii(buffer.data());
    buffer.clear();
    this->httpMethod = "HIBA1";
    int pos1 = st.indexOf("POST /");
    if (pos1 > -1) {
        this->httpMethod = "POST";
    } else {
        pos1 = st.indexOf("GET /");
        if (pos1 > -1) {
            this->httpMethod = "GET";
        }
    }
    if (this->httpMethod != "HIBA1") {
        int pos2 = st.indexOf(" HTTP/");
        if (pos2 > -1) {
            QString st2 = st.mid(pos1 + this->httpMethod.length() + 2, pos2 - pos1 - this->httpMethod.length() - 2);
//            qWarning("B003a, st:\n" + st.toAscii().replace("%", "%%"));
//            qWarning("B003b, st2: |" + st2.toAscii().replace("%", "%%") + "|");
            QByteArray st3 =  QByteArray::fromPercentEncoding(st2.toAscii());
//            qWarning("B003c, saHex(st3): " + saHex(st3).toAscii().replace("%", "%%"));
            sendMessage(QTextCodec::codecForMib(106)->toUnicode(st3));
        } else {
            this->httpMethod = "HIBA2";
        }
    }
    if (this->httpMethod.startsWith("HIBA")) {
        sendMessage(this->httpMethod);
    }
}

int TcpIPSocket::readDataIntoBuffer(int maxSize)
{
    if (maxSize > MaxBufferSize)
        return 0;

    int numBytesBeforeRead = buffer.size();
    if (numBytesBeforeRead == MaxBufferSize) {
        abort();
        return 0;
    }

    while (bytesAvailable() > 0 && buffer.size() < maxSize) {
        buffer.append(read(1));
    }
//    qWarning("B002");
    return buffer.size() - numBytesBeforeRead;
}

bool TcpIPSocket::sendMessage(QString message)
{
    QString body = "";
    if (this->httpMethod.startsWith("HIBA")) {
        body = this->httpMethod;
    } else {
        body = sz->ValaszFv(message).toUtf8();
    }
    if (this->httpMethod == "GET") {
        body =
            "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"></head><body><h1>"
            + body + "</h1></body></html>";
    }

    QString head =
            "HTTP/1.1 200 OK\n"
            "Date: Mon, 27 Jul 2018 12:28:53 GMT\n"
            "Server: Apache/2.2.14 (Win32)\n"
            "Last-Modified: Wed, 22 Jul 2018 12:15:56 GMT\n"
            "ETag: \"\"\n"
            "Vary: Authorization,Accept\n"
            "Accept-Ranges: bytes\n"
            "Content-Length: " + QString::number(body.toAscii().length()) + "\n"
            "Content-Type: text/html\n"
            "Connection: Closed\n"
            "\n";
    QByteArray msg = head.toAscii() + body.toAscii();
    return write(msg) == msg.size();
}

QString TcpIPSocket::saHex(QByteArray x)
{
    return x.toPercentEncoding().constData();
}
