﻿#include <QFileInfo>
#include <QTextStream>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "Szotar.h"

QString SZEPA1 = QString::fromUtf8("÷1");   //kínai szavakat választ el
QString SZEPA2 = QString::fromUtf8("÷2");   //1 szó jelentéseit választja el a nyers adatban és pin-ben
QString SZEPA3 = QString::fromUtf8("÷3");   //kínai szótagokat választ el,
QString SZEPA4 = QString::fromUtf8("÷");    //lokális (saját) szótárban tagol
QString SZEPA5 = QString::fromUtf8("÷5");    //latin karaktereket választ el
QString PINSZOKOZ = QString::fromUtf8("÷4");      //1 szó kiejtéseit választja el a pin-ben
QString SZEPA6 = QString::fromUtf8("÷6");    //Új FvChToEn formátum tagolása
QString SZEPAENT_SEC = QString::fromUtf8("÷ENT_SEQ");    //JMdict-ben a gloss végén ezután jön az ent_seq


extern void setParameter(QString *forras, QString parNev, QString ertek);
extern QString getParameter(QString *forras, QString parNev, QString defaultErtek = NULL);
extern QString iniFajlNev;
extern QString iniFajlSt;
extern MainWindow *w;
extern QString exePath;
extern void QStToFajl(QString fajlNev, QString st, bool kellBOM);
extern QString fajlToQString(QString fajlNev);
extern void setParameter(QString *forras, QString parNev, QString ertek);

bool kellNagybetusPin;

Szotar::Szotar()
{
    pinBeolvas();
    kellNagybetusPin = true;
}

Szotar::~Szotar()
{
}

QString Szotar::ValaszFv(QString kerdez) {
    return ValaszFv2(kerdez);
}

QString Szotar::ValaszFv2(QString kerdez) {
    QString kerdez2;
    QString valasz;
    if (kerdez.startsWith("Fv1Nyers|")) {
        kerdez2=kerdez.mid(9);
        valasz = Fv1Nyers(kerdez2);
    } else if (kerdez.startsWith("Fv1NyersB|")) {
            kerdez2=kerdez.mid(10);
            valasz = Fv1NyersB(kerdez2);
    } else if (kerdez.startsWith("FvMNyers|")) {
        kerdez2=kerdez.mid(9);
        valasz = FvMNyers(kerdez2);
    } else if (kerdez.startsWith("FvMNyersB|")) {
        kerdez2=kerdez.mid(10);
        valasz = FvMNyersB(kerdez2);
    } else if (kerdez.startsWith("FvChToEnNyers|")) {
        kerdez2=kerdez.mid(14);
        valasz = FvChToEnNyers(kerdez2);
    } else if (kerdez.startsWith("FvChToEn|")) {
        kerdez2=kerdez.mid(9);
        valasz = FvChToEn(kerdez2);
    } else if (kerdez.startsWith("FvChToPin|")) {
        kerdez2=kerdez.mid(10);
        valasz = FvChToPin(kerdez2);
    } else if (kerdez.startsWith("FvChToSt|")) {
        kerdez2=kerdez.mid(9);
        valasz = FvChToSt(kerdez2);
    } else if (kerdez.startsWith("FvPinToPin15|")) {
        kerdez2=kerdez.mid(13);
        valasz = PinToPin15(kerdez2);
    } else if (kerdez.startsWith("FvPin15ToPin|")) {
        kerdez2=kerdez.mid(13);
        valasz = Pin15ToPin(kerdez2);
    } else if (kerdez.startsWith("FvChToTr|")) {
        kerdez2=kerdez.mid(9);
        valasz = FvChToTr(kerdez2);
    } else if (kerdez.startsWith("FvJpToLat|")) {
        kerdez2=kerdez.mid(10);
        valasz = FvJpToLat(kerdez2);
    } else if (kerdez.startsWith("FvJpToEnNyers|")) {
        kerdez2=kerdez.mid(14);
        valasz = FvJpToEnNyers(kerdez2);
    } else if (kerdez.startsWith("FvJpToEn|")) {
        kerdez2=kerdez.mid(9);
        valasz = FvJpToEn(kerdez2);
    } else if (kerdez.startsWith("TESZT|")) {
        kerdez2=kerdez.mid(6);
        kerdez2 = kerdez2.replace("\"", "");
        QStringList stL = kerdez2.split(", ");

        //getTone:
//        if (stL.size() == 2 && stL.last().length()>0) {
//            valasz = getTone(stL[0]);
//        }

        //toneEsNagybetusKorrekcio:
        if (stL.size() == 2 && stL.last().length()>0) {
            valasz = toneEsNagybetusKorrekcio(stL[0], stL[1]);
        }

    } else if (kerdez.startsWith("CMD|")) {
        QString cmd = kerdez.split("|")[1];
        if (cmd == "lb") {
            valasz = localSzotarBeolvas();
        } else if (cmd.startsWith("lb ")) {
            valasz = localSzotarBeolvas(cmd.split(" ")[1], true);
        } else if (cmd == "gssh") {
            QString st = getParameter(&iniFajlSt, QString::fromUtf8("SAJÁT_SZÓTÁRFÁJL")).toUtf8();
            valasz = st.indexOf(":") == -1 ? exePath + st : st;
        } else {
            valasz = "- lb: localLista.txt, " + w->satr("mint saját szótár fájl beolvasása") + "\n" +
                    "- lb <fájl>: fájl, " + w->satr("mint saját szótár fájl beolvasása") + "\n" +
                    "- gssh: " + w->satr("saját szótár fájl helye");
        }
    } else {
        valasz = w->satr("_HIBA: Nincs megadva, hogy melyik függvényt használjuk") + " (Fv1Nyers, FvMNyers, FvChToEn, FvChToPin, FvChToSt, FvChToTr).";
    }
    return valasz;
}

QString Szotar::Nyers1Alap(QString kerdez, bool isPin15, bool csakCedictbenKeres) {
    QString er = "";
    QString st;
    talalHossz = 0;

    st = szotKeres(kerdez, csakCedictbenKeres); //A válasz: karSzam + SZEPA1 + találat!!!
    if (st == "0" + SZEPA1 + "-") {
        talalHossz = 1;
        er = kerdez.left(1);
    } else {
        QStringList stL = st.split(SZEPA1);
        talalHossz = stL[0].toInt();
        er = stL[1];
        //qWarning("er: " + er.toUtf8());
        if (!isPin15) { //kell pin15 -> pin átalakítás
            QString a ="";
            int pos0 = 0;
            int pos1 = er.indexOf("[", pos0);
            int pos2 = er.indexOf("]", pos1);
            while (pos0 < er.length()-1 && pos1 > -1 && pos2 > -1) {
                QString pin15 = er.mid(pos1+1, pos2-pos1-1);
                pin15 = pin15.replace(" ", SZEPA3);
                int i = pin15.split(SZEPA3).size();
                QString ch = "";
                int pos3 = pos1-1;
                while ( 0 < i) {
                    if (er.mid(pos3, 1) != " ") {
                        i--;
                        ch = er.mid(pos3,1) + ch;
                    }
                    pos3--;
                }
                pin15 = pin15 + SZEPA1;
                //qWarning("ch = " + ch.toLatin1() + ", pin15 = " + pin15.toUtf8());
                a = a + er.mid(pos0, pos1-pos0+1) + toneEsNagybetusKorrekcio(ch, pin15);
                //qWarning("a = " + a.toLatin1());
                pos0 = pos2;
                pos1 = er.indexOf("[", pos0);
                pos2 = er.indexOf("]", pos1);
            }
            er = a + er.mid(pos0);
        }
    }
    return er;
}

QString Szotar::Fv1Nyers(QString kerdez) {  //pin15 -ös tone-okat ad eredményül
    return Nyers1Alap(kerdez, true, false);
}

QString Szotar::Fv1NyersB(QString kerdez) { //pin tone-okat ad eredményül
    return Nyers1Alap(kerdez, false, false).replace(SZEPA3, "");
}

QString Szotar::NyersMAlap(QString kerdez0, bool isPin15, bool csakCedictbenKeres) {
    //isPin15==true: pin15 -ös tone-okat ad eredményül, egyébként normál tone-okat
    QString er = "";
    QStringList kerdezL = kerdez0.split(SZEPA1);

    for (int i=0; i<kerdezL.size(); i++) {
        do {
            if (er == "") {
                er = Nyers1Alap(kerdezL[i], isPin15, csakCedictbenKeres);
            } else {
                er = er + SZEPA1 + Nyers1Alap(kerdezL[i], isPin15, csakCedictbenKeres);
            }
            kerdezL[i] = kerdezL[i].right(kerdezL[i].length() - talalHossz);
        } while (kerdezL[i].length()>0);
    }
    return er;
}

QString Szotar::FvMNyers(QString kerdez0) { //pin15 -ös tone-okat ad eredményül
    return NyersMAlap(kerdez0, true, false);
}

QString Szotar::FvMNyersB(QString kerdez) { //pin tone-okat ad eredményül
    return NyersMAlap(kerdez, false, false).replace(SZEPA3, "");
}

QString Szotar::FvChToSt(QString kerdez0) { //-> standard
    QString er = "";
    QString st = NyersMAlap(kerdez0, true, true);;
    QStringList stL = st.split(SZEPA1);
    for (int i=0; i<stL.length(); i++) {
        if (stL.at(i).contains(" ")) {
            QString st2 = stL.at(i).split(" ").at(1);
            er = er + st2;
        } else {
            er = er + stL.at(i);
        }
    }
    return er;
}

QString Szotar::FvChToTr(QString kerdez0) {
    QString er = "";
    QString st = NyersMAlap(kerdez0, true, true);
    QStringList stL = st.split(SZEPA1);
    for (int i=0; i<stL.length(); i++) {
        QString st2 = stL.at(i).split(" ").at(0);
        er = er + st2;
    }
    return er;
}


QString Szotar::szotKeres(QString kerdez0, bool csakCedictbenKeres){
    //FvChToSt vagy FvChToTr használatkor csakCedictbenKeres == true
    //A válasz: karSzam + SZEPA1 + találat!!!
    QString localEr = "";
    int localHossz = 0;
    QString er;
	QString kerdez = kerdez0.toLower();
    //qWarning("szotKeres: kerdez = " + kerdez.toUtf8());
    long idx = -1;
    if (w->ui->checkBox_2->isChecked() && csakCedictbenKeres == false) {
        idx = localL.keresSzotar(kerdez);
        if (idx > -1) {
            QString k = localL.getKey(idx);
            if (k == kerdez.left(k.length())) {
                QString ny = localL.getValue(idx);
                localHossz = k.length();
                localEr = QString::number(localHossz) + SZEPA1 + ny;
            }
        }
    }

    idx = listL.keresSzotar(kerdez);
    if (idx == -1) {
        if (localEr == "") {
            er = "0" + SZEPA1 + "-";
        } else {
            er = localEr;
        }
    } else {
        QString k = listL.getKey(idx);
        int hossz = k.length();
        if (hossz <= localHossz || k != kerdez.left(hossz)) {
            if (localEr == "") {
                er = "0" + SZEPA1 + "-";
            } else {
                er = localEr;
            }
        } else {
            QString ny = nyersL[listL.getValue(idx)];
            if (!kellNagybetusPin) {
                bool vanKisbetusPin = false;
                QStringList jelentesek = ny.split(SZEPA2);
                for (int i=0; i<jelentesek.size(); i++) {
                    QString st = jelentesek[i].split("[")[1];
                    st = st.left(1);
                    if (st.toLower() != st && vanKisbetusPin) {
                        jelentesek.removeAt(i);
                    } else {
                        vanKisbetusPin = true;
                    }
                }
                ny = jelentesek.join(SZEPA2);
            }
            er = QString::number(k.length()) + SZEPA1 + ny;
        }
    }
    er = er.trimmed();
    return er;
}

QString Szotar::nyersetTagol(QString kerdez){
    QString st = "";
    bool pinbenVagyunk = false;
    for (int i=0; i<kerdez.length(); i++) {
        QString b = kerdez.mid(i,1);
        if (b == "[") {
            pinbenVagyunk = true;
            st = st + b;
        } else if (b == "]") {
            pinbenVagyunk = false;
            st = st + b;
        } else if (b == " ") {
            if (pinbenVagyunk) {
                st = st + SZEPA3;
            } else {
                st = st + b;
            }
        } else {
            st = st + b;
        }
    }
    return st;
}

bool Szotar::szotarBeolvas() {
    bool ok = true;
    if (eredetiCedictSzotarFajlNev != w->ui->bTextEdit1->toPlainText()) {
        if (szotarBeolvas(w->ui->bTextEdit1->toPlainText(), 0)) {
            eredetiCedictSzotarFajlNev = w->ui->bTextEdit1->toPlainText();
            setParameter(&iniFajlSt, QString::fromUtf8("CEDICT_SZÓTÁRFÁJL"), w->ui->bTextEdit1->toPlainText());
            QStToFajl(iniFajlNev, iniFajlSt, false);
        } else {
            ok = false;
        }
    }
    if (eredetiJMdictSzotarFajlNev != w->ui->bTextEdit1_2->toPlainText()) {
        if (szotarBeolvas(w->ui->bTextEdit1_2->toPlainText(), 1)) {
            eredetiJMdictSzotarFajlNev = w->ui->bTextEdit1_2->toPlainText();
            setParameter(&iniFajlSt, QString::fromUtf8("JMDICT_SZÓTÁRFÁJL"), w->ui->bTextEdit1_2->toPlainText());
            QStToFajl(iniFajlNev, iniFajlSt, false);
        } else {
            ok = false;
        }
    }
    return ok;
}

bool Szotar::szotarBeolvas(QString fajlNev0, int type) {    //type: 0: Cedict, 1: JMdict
    QString fajlNev = fajlNev0;
    if (fajlNev.indexOf(":") == -1) {
        fajlNev = exePath + fajlNev;
    }
    QFile file(fajlNev);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        QString kiir = w->satr("HIBA (Szotar::szotarBeolvas): nem sikerült beolvasni ezt a szótárfájlt:");
        w->konzolOut(
                kiir + " " + fajlNev, true);
        qWarning(kiir.toUtf8() + " " + fajlNev.toUtf8());
        return false;
    }
    QByteArray line;
    QString lineSt = "";
    long recordCount = 0;
    if (type == 0) {       //CEDICT beolvasás:
        listL.torol();
        nyersL.clear();
        long i1, i2;
        while (!file.atEnd()) {
            i1 = i2 = -1;
            line = file.readLine();
            lineSt = QString::fromUtf8(line.data());
            if (lineSt.left(1) != "#" && lineSt.length()>5) {
                QStringList stL = lineSt.split(" ");
                if (stL[0].length() > 0) {
                    int idx = listL.keresBeszur(stL[0]);
                    if (idx < listL.size() && listL.getKey(idx) == stL[0].toLower()) {
                        i1 = listL.getValue(idx);
                        QString st = stL[2];
                        st = st.left(2);    //[ utánni karakter kell!!!
                        if (st.toUpper() == st) {   //nagybetűs pin hátra
                            nyersL[i1] = nyersL[i1] + SZEPA2 + nyersetTagol(lineSt);
                        } else {
                            nyersL[i1] = nyersetTagol(lineSt) + SZEPA2 + nyersL[i1];
                        }
                    } else {
                        nyersL.append(nyersetTagol(lineSt));
                        listL.beszur(idx, stL[0], nyersL.count()-1);
                    }
                }
                if (stL[1].length() > 0 && stL[0] != stL[1]) {
                    int idx = listL.keresBeszur(stL[1]);
                    if (idx <listL .size() && listL.getKey(idx) == stL[1].toLower()) {
                        i2 = listL.getValue(idx);
                        QString st = stL[2];
                        st = st.left(2);    //[ utánni karakter kell!!!
                        if (st.toUpper() == st) {   //nagybetűs pin hátra
                            nyersL[i2] = nyersL[i2] + SZEPA2 + nyersetTagol(lineSt);
                            //qWarning("st: " + st.toUtf8() + ", nyersL[i2]_1: " + nyersL[i2].toUtf8());
                        } else {
                            nyersL[i2] = nyersetTagol(lineSt) + SZEPA2 + nyersL[i2];
                            //qWarning("st: " + st.toUtf8() + ", nyersL[i2]_2: " + nyersL[i2].toUtf8());
                        }
                    } else {
                        nyersL.append(nyersetTagol(lineSt));
                        listL.beszur(idx, stL[1], nyersL.count()-1);
                        //qWarning("nyersL.last(): " + nyersL.last().toUtf8());
                    }
                }
            }
        }
        recordCount = listL.size();
        QString kiir = w->satr("Ez a szótárfájl beolvasva:") + " " +
                fajlNev + ", " + w->satr("rekordok száma:") + " " + QString::number(recordCount) + ".";
        w->konzolOut(kiir, true);
        qWarning(kiir.toLatin1());
        file.close();
    } else if (type == 1) {       //JMdict beolvasás:
        //syllabaryL feltöltése:
        if (syllabaryL.size()==0) {
            QString fileSyllNev = exePath + "syllabary.cfg";
            QFile fileSyll(fileSyllNev);
            if (!fileSyll.open(QIODevice::ReadOnly | QIODevice::Text)) {
                QString kiir = w->satr("HIBA (Szotar::szotarBeolvas): nem sikerült beolvasni ezt a fájlt:");
                w->konzolOut(
                        kiir + " " + fileSyllNev, true);
                qWarning(kiir.toUtf8() + " " + fileSyllNev.toUtf8());
                return false;
            }
            QByteArray lineS;
            QString lineStS;
            do {
                lineS = fileSyll.readLine();
                lineStS = QString::fromUtf8(lineS.data());
                lineStS = lineStS.trimmed();
                if (!lineStS.startsWith("#") && lineStS.size()>0) {
                    int pos = lineStS.indexOf(":");
                    syllabaryL.beszur(lineStS.left(pos), lineStS.mid(pos+1).split("#")[0].trimmed());
                }
            } while (!fileSyll.atEnd());
        }

/*
        //entityL feltöltése:
        line = file.readLine();
        lineSt = QString::fromUtf8(line.data());
        QString sorVeg = lineSt.right(1);
        while (!file.atEnd() && (!lineSt.startsWith("<!ENTITY ") || !lineSt.endsWith(""">" + sorVeg))) {   //első <!ENTITY keresése:
            line = file.readLine();
            lineSt = QString::fromUtf8(line.data());
        }
        while (!file.atEnd() && lineSt.startsWith("<!ENTITY ") && lineSt.endsWith(""">" + sorVeg)) {   //<!ENTITY -s sorok beolvasása:
            int pos = lineSt.indexOf(" ", 9);
            entityL[lineSt.mid(9, pos-9)] = lineSt.mid(pos+2, lineSt.size()-5-pos);
            line = file.readLine();
            lineSt = QString::fromUtf8(line.data());
        }
*/

        //l. d:\Joci\Prog\CEDICTkezeles\CedictServer\Segéd.doc
        line = "";
        lineSt = QString::fromUtf8(line.data());
        QRegExp rx;
        rx.setMinimal(true);
        QString ent_seq;
        QStringList kebL;
        QStringList rebL;
        QString reb;
        QString gloss;
        jpListL.torol();
        jpNyersL.clear();

        while (!file.atEnd()) {
            do {
                line = file.readLine();
                lineSt = QString::fromUtf8(line.data());
            } while (!file.atEnd() && (!lineSt.startsWith("<entry>")));
            ent_seq = "";
            kebL.clear();
            rebL.clear();
            gloss = "";
            do {
                line = file.readLine();
                lineSt = QString::fromUtf8(line.data());

                rx.setPattern("<ent_seq.*>(.*)<\\/ent_seq>");
                if (rx.indexIn(lineSt) != -1) {
                    if (ent_seq != "") {
                        exit(-10);
                    } else {
                        ent_seq = rx.cap(1);
                        continue;
                    }
                }

                rx.setPattern("<keb.*>(.*)<\\/keb>");
                if (rx.indexIn(lineSt) != -1) {
                   kebL << rx.cap(1).trimmed();
                   continue;
                }

                rx.setPattern("<reb.*>(.*)<\\/reb>");
                if (rx.indexIn(lineSt) != -1) {
                   rebL << rx.cap(1).trimmed();
                   continue;
                }

                rx.setPattern("<sense.*>.*");
                if (rx.indexIn(lineSt) != -1) {
                    if (gloss != "") {
                        gloss += "; ";
                    }
                   continue;
                }

                rx.setPattern("<gloss.*>(.*)<\\/gloss>");
                if (rx.indexIn(lineSt) != -1) {
                    if (gloss != "" && !gloss.endsWith("; ")) {
                        gloss += ", ";
                    }
                   gloss += rx.cap(1).trimmed();
                   continue;
                }

            } while (!file.atEnd() && (!lineSt.startsWith("</entry>")));

            lineSt = "";
            if (kebL.size() == 0) {
                kebL = rebL;
                for (int i=0; i<kebL.size(); i++) {
                    QString lat = rebToLat(rebL[i]);
                    jpSzotBerak(kebL[i], lat, gloss + SZEPAENT_SEC + ent_seq);
                    lineSt += kebL[i] + " - " + lat + " - " + gloss + SZEPAENT_SEC + ent_seq + "\n";
                }
            } else {
                for (int i=0; i<rebL.size(); i++) {
                     rebL[i] = rebToLat(rebL[i]);
                }
                rebL.removeDuplicates();
                reb = rebL.join("&");
                for (int i=0; i<kebL.size(); i++) {
                    jpSzotBerak(kebL[i], reb, gloss + SZEPAENT_SEC + ent_seq);
                    lineSt += kebL[i] + " - " + reb + " - " + gloss + SZEPAENT_SEC + ent_seq + "\n";
                }
            }
        }
        recordCount = jpListL.size();
        QString kiir = w->satr("Ez a szótárfájl beolvasva:") + " " +
                fajlNev + ", " + w->satr("rekordok száma:") + " " + QString::number(recordCount) + ".";
        w->konzolOut(kiir, true);
        qWarning(kiir.toLatin1());
        file.close();
    }


//    qWarning("Szotar::szotarBeolvas(): syllabaryL:");
//    for(int i=0; i<syllabaryL.size(); i++) {
//        qWarning(syllabaryL.getKey(i).toUtf8() + ": " + syllabaryL.getValue(i).toUtf8());
//    }
//    qWarning("Szotar::szotarBeolvas(): " + entityL["gikun"].toUtf8());

    return true;
}


bool Szotar::localSzotarBeolvas() {
    QString fajlNev = w->ui->bTextEdit2->toPlainText();
    return localSzotarBeolvas(fajlNev, false);
}


bool Szotar::localSzotarBeolvas(QString fajlNev0, bool kellAdmin) {
    //a lokális sor formátuma: ch÷pin÷fordítás

    QString fajlNev = fajlNev0;
    if (fajlNev.indexOf(":") == -1) {
        fajlNev = exePath + fajlNev;
    }
    if (!QFile(fajlNev).exists()) {
        QFile::copy(exePath + "localLista.txt.template", fajlNev);
    }
    QFile file(fajlNev);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        w->ui->bTextEdit2->setPlainText(eredetiLocalSzotarFajlNev);
        QString msgSt =
                w->satr("HIBA (Szotar::szotarBeolvas): nem sikerült beolvasni ezt a saját szótárfájlt:") +
                " " +
                fajlNev;
        w->konzolOut(msgSt, true);
        return false;
    }
    if (kellAdmin) {
        setParameter(&iniFajlSt, QString::fromUtf8("SAJÁT_SZÓTÁRFÁJL"), fajlNev0);
        QStToFajl(iniFajlNev, iniFajlSt, false);
        w->bTextEditSetPlainText(w->ui->bTextEdit2, fajlNev0);
    }

    localL.torol();
    while (!file.atEnd()) {
        QByteArray line = file.readLine();
        QString st = QString::fromUtf8(line);
        //qWarning("st: " + st.toUtf8());
        st = st.trimmed();
        if (st.left(1) != "#") {
            QStringList stL = st.split(SZEPA4);
            if (stL[0].length() > 0) {
                long idx = localL.keresBeszur(stL[0]);
                if (idx < localL.size() && localL.getKey(idx) == stL[0]) {
                    QString st2 = localL.getValue(idx);
                    //qWarning("st2_1: " + st2.toUtf8());
                    QString st3 = st2.split(" ")[2];
                    st3 = st3.left(1);
                    if (st3.toUpper() == st3) {   //nagybetűs pin hátra
                        localL.setValue(idx, nyersetTagolLocal(st) + SZEPA2 +
                                localL.getValue(idx));
                    } else {
                        localL.setValue(idx, localL.getValue(idx) + SZEPA2 +
                                nyersetTagolLocal(st));
                    }
                    st = localL.getValue(idx);
                    //qWarning("st_2: " + st.toUtf8());
                } else {
                    localL.beszur(idx, stL[0], nyersetTagolLocal(st));
                }
            }
        }
    }
    file.close();
    eredetiLocalSzotarFajlNev = fajlNev0;

    w->localSzotarBeolvasasKesz2();
    QString kiir = w->satr("Ez a saját szótárfájl beolvasva:") + " " +
            eredetiLocalSzotarFajlNev +
            ", " + w->satr("rekordok száma:") + " " + QString::number(localL.size()) + ".";
    w->konzolOut(kiir, true);
    qWarning(kiir.toLatin1());
    return true;
}

QString Szotar::nyersetTagolLocal(QString kerdez){
    //az új lokális sor formátuma: ch÷pin÷ford,
    //"Xiao3Ming2" -> "Xiao3" + SZEPA3 + "Ming2"
    //"Xiao3 Ming2" -> "Xiao3" + PINSZOKOZ + "Ming2"
    QString er = "";
    QStringList stL = kerdez.split(SZEPA4);

    QString pin15 = PinToPin15(stL[1]);
    //qWarning("nyersetTagolLocal: pin15 = " + pin15.toUtf8());
    er = " " + stL[0] + " [" + pin15 + "] /" + stL[2] + "/\n";
    return er;
}

void Szotar::pinBeolvas() {
    QString fajlNev = exePath + "Pin15Lista.txt";
    QFile fajl(fajlNev);
    if (!fajl.open(QIODevice::ReadOnly)) {
        qWarning("\nHIBA: pinBeolvas, ez nincs meg: " + fajlNev.toUtf8());
        exit(233);
    }
    while (!fajl.atEnd()) {
        QByteArray line = fajl.readLine();
        QString st = QString::fromUtf8(line);
        st = st.trimmed();
        QStringList stL = st.split(" ");
        pinPin15L.beszur(stL[0], stL[1]);
        pin15PinH.insert(stL[1], stL[0]);
    }
}

QString Szotar::PinToPin15(QString x0) {
    QString er = "";
    QString x = x0;
    while (x.length() > 0) {
        if (x.left(1) == " ") {
            er = er + PINSZOKOZ;
            x = x.right(x.length() - 1);
        } else {
            long idx = pinPin15L.keresSzotar(x);
            if (idx > -1) {
                QString key = pinPin15L.getKey(idx);
                QString value = pinPin15L.getValue(idx);
                if (x.left(1).toUpper() == x.left(1)) {
                    value = value.left(1).toUpper() + value.right(value.length()-1);
                }
                er = er + value + SZEPA3;
                x = x.right(x.length() - key.length());
            } else {
                er = er + x.left(1) + SZEPA3;
                x = x.right(x.length() - 1);
            }
        }
    }
    er.chop(SZEPA3.length());   //utolsó SZEPA3 levétele
    return er;
}

QString Szotar::Pin15ToPin(QString x0) {
    //1 szótag (!) pin15 -> pin átalakítása
    QString x = x0.toLower();
    //qWarning("Pin15ToPin: x_1 = " + x.toUtf8());
    if (x0 != "" && pin15PinH.contains(x)) {
        x = pin15PinH.value(x);
        //qWarning("Pin15ToPin: x_2 = " + x.toUtf8());
        if (x0.left(1).toLower() != x0.left(1)) {
            x = x.left(1).toUpper() + x.right(x.length()-1);
        }
        return x;
    } else {
        return x0;
    }
}

QString Szotar::FvChToPin(QString kerdez0) {
    QString kerdez = kerdez0;
    QString pin15 = "";
    QString nyers;

    //qWarning("FvChToPin - kerdez0 = " + kerdez0.toUtf8());
    bool kellNagybetusPin_e = kellNagybetusPin;
    kellNagybetusPin = w->ui->checkBox_4->isChecked();
    while (kerdez.length() > 0) {
        if (QRegExp("[AaQq]").exactMatch(kerdez.left(1))) {
            nyers = kerdez.left(1);
            kerdez = kerdez.right(kerdez.length() - 1);
            nyers = nyers + SZEPA5;
        } else {
            QString ny = Nyers1Alap(kerdez, true, false);
            //qWarning("ny_1:" + ny.toUtf8());
            if (ny.length()>1) {
                QStringList nyersL = ny.split(SZEPA2);
                int hossz = nyersL.at(0).split(" ").at(1).length();
                ny = "";
                for (int i=0; i < nyersL.size(); i++) {
                    QString ny2 = nyersL[i].split("[")[1];
                    ny = ny + SZEPA2 + ny2.split("]")[0];
                }
                ny = ny.right(ny.length()-SZEPA2.length());  //első & leszedése
                //qWarning("ny_2:" + ny.toUtf8());


                //pin rendezés ("b÷2a÷2c" -> "a÷2b÷2c"):
                QStringList pinL = ny.split(SZEPA2);
                pinL.sort();
                //duplikációk kiszedése ("a÷2b÷2b÷2c" -> "a÷2b÷2c"):
                for (int i=1; i<pinL.size(); i++) {
                    if (pinL[i-1] == pinL[i]) {
                        pinL.removeAt(i);
                        i--;
                    }
                }
                //nagybetűs kiszedése v. hátra ("A÷2B÷2a÷2b" -> "a÷2b÷2A÷2B"):
                //qWarning("pinL.join(\"" + SZEPA2.toUtf8() + "\"): " + pinL.join(SZEPA2).toUtf8());
                for (int i=0; i<pinL.size()-1; i++) {
                    QString tag = pinL[i];
                    //qWarning("tag: " + tag.toUtf8());
                    if (tag.toLower() != tag &&
                            (pinL.last().toLower() == pinL.last() || tag > pinL.last())) {
                        pinL.removeAt(i);
                        pinL.append(tag);
                        i--;
                    }
                    //qWarning("i: %d, pinL.join(\"" + SZEPA2 + "\"): " + pinL.join(SZEPA2).toUtf8(), i);
                }
                ny = pinL.join(SZEPA2);
                //qWarning("ny_3:" + ny.toUtf8());

                ny = ny.trimmed();
                kerdez = kerdez.right(kerdez.length() - hossz);
                nyers = ny + SZEPA1;
            } else {
                nyers = kerdez.left(1) + SZEPA5;
                kerdez = kerdez.right(kerdez.length() - 1);
            }
        }
        pin15 = (pin15 == "") ? nyers : pin15 + nyers;
    }
    //pin15 = pin15.right(pin15.length()-SZEPA1.length()); //SZEPA1-et levesszük az elejéről
    //Itt pin15 kész.

    //tone és nagybetűs korrekció:
    if (pin15.length() > kerdez0.length()) {
//        qWarning("FvChToPin: pin15_1 = " + pin15.toUtf8());
        pin15 = toneEsNagybetusKorrekcio(kerdez0, pin15);
//        qWarning("FvChToPin: pin15_2 = " + pin15.toUtf8());
    }

    kellNagybetusPin = kellNagybetusPin_e;

    return pin15;
}

QString Szotar::FvChToEnNyers(QString kerdez) {
    //Az angol fordítás /-eket tartalmaz, mint a nyersben
    QString er = "";
    QString a = FvMNyersB(kerdez);
    QStringList szoTomb = a.split(SZEPA1);
    for (int i=0; i<szoTomb.size(); i++) {
        if (szoTomb.at(i).length() == 0) {
            //semmi
        } else if (szoTomb.at(i).length() == 1) {
            if (er != "" && i>0 && szoTomb.at(i-1).length() > 1) {
                er = er + "\n";
            }
            er = er + szoTomb.at(i);
        } else {
            if (i>0 && szoTomb.at(i-1).length() == 1) {
                er = er + SZEPA6 + SZEPA6;
            }
            if (er != "") {
                er = er + "\n";
            }
            QStringList jelentesTomb = szoTomb.at(i).split(SZEPA2);
            for (int j=0; j<jelentesTomb.size(); j++) {
                QString st = jelentesTomb[j];

                if (j == 0) {
                    er = er + st.split(" ")[1];
                } else {
                    //er = er + "\n";
                }

                QString pin = st.split("[")[1];
                pin = pin.split("]")[0];
                er = er + SZEPA6 + pin + SZEPA6;

                int pos = st.indexOf("]");
                er = er + st.mid(pos+1);
            }
        }
    }
    if (szoTomb.size()>0 && szoTomb.last().length() == 1) {
        er = er + SZEPA6 + SZEPA6;
    }
    er = er.replace(SZEPA3, "");
    //er = er.replace(PINSZOKOZ, " ");
    er = er.trimmed();
    return er;
}

QString Szotar::FvChToEn(QString kerdez) {
    //Az angol fordítás /-ek helyett "; "-eket tartalmaz

    QString er = "";
    QString a = FvMNyersB(kerdez);
    QStringList szoTomb = a.split(SZEPA1);
    for (int i=0; i<szoTomb.size(); i++) {
        if (szoTomb.at(i).length() == 0) {
            //semmi
        } else if (szoTomb.at(i).length() == 1) {
            if (er != "" && i>0 && szoTomb.at(i-1).length() > 1) {
                er = er + "\n";
            }
            er = er + szoTomb.at(i);
        } else {
            if (i>0 && szoTomb.at(i-1).length() == 1) {
                er = er + SZEPA6 + SZEPA6;
            }
            if (er != "") {
                er = er + "\n";
            }
            QStringList jelentesTomb = szoTomb.at(i).split(SZEPA2);
            for (int j=0; j<jelentesTomb.size(); j++) {
                QString st = jelentesTomb[j];

                if (j == 0) {
                    er = er + st.split(" ")[1];
                } else {
                    //er = er + "\n";
                }

                QString pin = st.split("[")[1];
                pin = pin.split("]")[0];
                er = er + SZEPA6 + pin.replace(PINSZOKOZ," ") + SZEPA6;

                int pos = st.indexOf("]");
                QString st2 = st.mid(pos+1).trimmed();
                if (st2.left(1) == "/") {
                    st2 = st2.right(st2.length() - 1);
                }
                if (st2.right(1) == "/") {
                    st2 = st2.left(st2.length() - 1);
                }
                er = er + st2.replace("/", "; ").trimmed();
            }
        }
    }
    if (szoTomb.size()>0 && szoTomb.last().length() == 1) {
        er = er + SZEPA6 + SZEPA6;
    }
    er = er.replace(SZEPA3, "");
    //er = er.replace(PINSZOKOZ, " ");
    er = "\n" + er + "\n";
    QString irj = QString::fromUtf8(" _,.?!\"/+-&@()%$#:;{}<>='*、，。？！；：》《（）");
    for (int i=0; i<irj.length(); i++) {
        er = er.replace("\n" + irj[i] + SZEPA6 + SZEPA6 + "\n", "\n");
    }
    er = er.trimmed();
    return er;
}

QString Szotar::toneEsNagybetusKorrekcio(QString ch, QString pin15_0) {
    //paraméter pl1.: ("我", "wo3")
    //paraméter pl2.: ("小明喜欢唱歌。", "Xiao3÷3 ÷3Ming2÷1xi3÷3huan5÷1chang4÷3ge1÷1.")

    //qWarning("(ch, pin15_0): (" + ch.toUtf8() + ", " + pin15_0.toUtf8() + ")");
    QString pin15 = pin15_0;

    //pin15 szétszedése:
    QList<QStringList> pin15L;
    QStringList szepaL;
    int cSzoKezdet = 0;
    int c = 0;   //ch-ban a karakter pozíció
    int p1 = 0;
    int p2;
    pin15L.append(QStringList());
    for (int i=0; i<pin15.length(); i++) {
        if (pin15.mid(i, SZEPA1.length()) == SZEPA1) {
            p2 = i;
            QString st = pin15.mid(p1, p2-p1);
            //qWarning("st_1 = " + st.toUtf8());
            pin15L[c].append(st);
            szepaL.append(SZEPA1);
            c++;
            if (c >= pin15L.size()) {
                pin15L.append(QStringList());
            }
            cSzoKezdet = c;
            i += SZEPA1.length() - 1;
            p1 = i + 1;
        } else if (pin15.mid(i, SZEPA5.length()) == SZEPA5) {
            p2 = i;
            QString st = pin15.mid(p1, p2-p1);
            //qWarning("st_2 = " + st.toUtf8());
            pin15L[c].append(st);
            szepaL.append(SZEPA5);
            c++;
            if (c >= pin15L.size()) {
                pin15L.append(QStringList());
            }
            cSzoKezdet = c;
            i += SZEPA5.length() - 1;
            p1 = i + 1;
        } else if (pin15.mid(i, SZEPA2.length()) == SZEPA2) {
            p2 = i;
            QString st = pin15.mid(p1, p2-p1);
            //qWarning("st_3 = " + st.toUtf8());
            pin15L[c].append(st);
            c = cSzoKezdet;
            i += SZEPA2.length() - 1;
            p1 = i + 1;
        } else if (pin15.mid(i, SZEPA3.length()) == SZEPA3) {
            p2 = i;
            QString st = pin15.mid(p1, p2-p1);
            //qWarning("st_4 = " + st.toUtf8());
            pin15L[c].append(st);
            if (pin15L[c].size() == 1) {
                szepaL.append(SZEPA3);
            }
            c++;
            if (c >= pin15L.size()) {
                pin15L.append(QStringList());
            }
            i += SZEPA3.length() - 1;
            p1 = i + 1;
        } else {
            //semmi
        }
    }
    pin15L.removeLast();    //ez egy üres elem, ami nem kell
    //szepaL kiírás:
//    QString kiirSt = QString::fromUtf8("toneEsNagybetusKorrekcio_0 - szepaL: |");
//    for (int i=0; i<szepaL.size(); i++) {
//        kiirSt = kiirSt + szepaL[i] + "|";
//    }
//    kiirSt = kiirSt + "\n";
//    qWarning(kiirSt.toUtf8());
//    if (ch.length() != pin15L.size() || ch.length()!= szepaL.size()) {
//        qWarning("ch.lenght() = %i, pin15L.size() = %i, szepaL.size() = %i",
//                ch.length(), pin15L.size(), szepaL.size());
//    }

    //kiírás
    //Pin15LKiiras(&pin15L, QString::fromUtf8("toneEsNagybetusKorrekcio_1 - pin15 szétszedés:"));

    //tone korrekció:
    for (int i=0; i<ch.length(); i++) {
        if (ch.mid(i, 1) == QString::fromUtf8("一") && pin15L[i][0].length()>1) {
            if (i>0 && (
                    ch.mid(i-1, 1) == QString::fromUtf8("第")
                    || ch.mid(i-1, 1) == QString::fromUtf8("尤"))) {
                for (int j=0; j<pin15L[i].size(); j++) {
                    pin15L[i][j] = setTone(pin15L[i][j], "1");
                }
                //qWarning("di4 yi1, you2 yi1");
            } else {
                if (i < pin15L.size() - 1) {
                    for (int j=0; j<qMax(pin15L[i].size(), pin15L[i+1].size()); j++) {
                        if (j < pin15L[i].size() && getTone(pin15L[i][j]) == "1") {
                            if (j < pin15L[i+1].size()) {
                                QString tone = getTone(pin15L[i+1][j]);
                                if (tone == "4") {
                                    pin15L[i][j] = setTone(pin15L[i][j], "2");
                                    //qWarning("yi2 hui4");
                                } else if (QString("123").indexOf(tone) > -1) {
                                    pin15L[i][j] = setTone(pin15L[i][j], "4");
                                    //qWarning("yi4 zhang1");
                                }
                            }
                        } else {
                            QString tone = getTone(pin15L[i+1][j]);
                            if (tone == "4") {
                                pin15L[i].append("yi2");
                                //qWarning("yi2 hui4 (append)");
                            } else if (QString("123").indexOf(tone) > -1) {
                                pin15L[i].append("yi4");
                                //qWarning("yi4 zhang1 (append)");
                            }
                        }
                    }
                }
            }
        } else if (ch.mid(i, 1) == QString::fromUtf8("不") && pin15L[i][0].length()>1) {
            if (i < pin15L.size() - 1) {
                for (int j=0; j<qMax(pin15L[i].size(), pin15L[i+1].size()); j++) {
                    if (j < pin15L[i].size()) {
                        if (j < pin15L[i+1].size()) {
                            QString tone = getTone(pin15L[i+1][j]);
                            //qWarning("tone = " + tone.toUtf8());
                            if (tone == "4") {
                                pin15L[i][j] = setTone(pin15L[i][j], "2");
                                //qWarning("bu2 hui4");
                            } else {
                                pin15L[i][j] = setTone(pin15L[i][j], "4");
                                //qWarning("bu4 zhang1");
                            }
                        }
                    } else {
                        QString tone = getTone(pin15L[i+1][j]);
                        if (tone == "4") {
                            pin15L[i].append("bu2");
                            //qWarning("bu2 hui4 (append)");
                        } else {
                            pin15L[i].append("bu4");
                            //qWarning("bu4 zhang1 (append)");
                        }
                    }
                }
            }
        }
    }
    //kiírás
    //Pin15LKiiras(&pin15L, QString::fromUtf8("toneEsNagybetusKorrekcio_2 - tone korrekció:"));

    //nagybetűs:
    bool elsoPinKarakterNagybetus = false;
    bool kovPinKarakterNagybetus = false;
    static const QChar unicode = 0xFF;
    for (int i=0; i<ch.length(); i++) {
        if (i > 0 && (QString::fromUtf8("。？！").indexOf(ch.mid(i-1, 1)) > -1 ||
                kovPinKarakterNagybetus)) {
            elsoPinKarakterNagybetus = true;
            kovPinKarakterNagybetus = true;
            if (QString::fromRawData(&unicode, 1) < ch.mid(i, 1) &&
                    kovPinKarakterNagybetus) {
                for (int j=0; j<pin15L[i].size(); j++) {
                    pin15L[i][j] = nagybetusTone(pin15L[i][j]);
                }
                kovPinKarakterNagybetus = false;
            }
        }
    }
    if (elsoPinKarakterNagybetus ||
            QString::fromUtf8("。？！").indexOf(ch.mid(ch.length()-1, 1)) > -1)    {
        for (int j=0; j<pin15L[0].size(); j++) {
            pin15L[0][j] = nagybetusTone(pin15L[0][j]);
        }
    }
    //kiírás
    //Pin15LKiiras(&pin15L, QString::fromUtf8("toneEsNagybetusKorrekcio_3 - nagybetűs:"));

    //pin15 -> pin:
    for (int i=0; i<pin15L.size(); i++) {
        for (int j=0; j<pin15L[i].size(); j++) {
            QString st = pin15L[i][j];
            if (st.startsWith(PINSZOKOZ)) {
                pin15L[i][j] = PINSZOKOZ + Pin15ToPin(st.right(st.length() - PINSZOKOZ.length()));
            } else {
                pin15L[i][j] = Pin15ToPin(st);
            }
        }
    }
    //kiírás
    //Pin15LKiiras(&pin15L, QString::fromUtf8("toneEsNagybetusKorrekcio_4 - pin15 -> pin:"));

    //pin15 összerakása:
    pin15 = "";
    cSzoKezdet = 0;
    c = 0;
    int d = 0;
    for (int i=0; i<szepaL.size(); i++) {
        pin15 = pin15 + pin15L[c][d];
        //qWarning("pin15 = " + pin15.toUtf8());
        if (szepaL[i] == SZEPA1) {
            if (d < pin15L[cSzoKezdet].size() - 1) {
                d++;
                c = cSzoKezdet;
                i = c - 1;
                pin15 = pin15 + SZEPA2;
            } else {
                pin15 = pin15 + SZEPA1;
                c++;
                cSzoKezdet = c;
                d = 0;
            }
        } else if (szepaL[i] == SZEPA5) {
            if (d < pin15L[cSzoKezdet].size() - 1) {
                d++;
                c = cSzoKezdet;
                i = c - 1;
                pin15 = pin15 + SZEPA2;
            } else {
                pin15 = pin15 + SZEPA5;
                c++;
                cSzoKezdet = c;
                d = 0;
            }
        } else if (szepaL[i] == SZEPA3) {
            pin15 = pin15 + SZEPA3;
            c++;
        }
    }
    pin15.chop(SZEPA1.length());    //SZEPA1 -et levesszük a végéről
    //qWarning("toneEsNagybetusKorrekcio: 5 - pin15 = " + pin15.toUtf8());

    pin15 = pin15.replace(PINSZOKOZ, " ");
    pin15 = pin15.replace(SZEPA5, "");
    pin15 = pin15.replace(SZEPA3, "");
    pin15 = pin15.replace(SZEPA2, "&");
    pin15 = pin15.replace(SZEPA1, " ");
    pin15 = pin15.replace(QString::fromUtf8("。"), ". ");
    pin15 = pin15.replace(QString::fromUtf8("？"), "?");
    pin15 = pin15.replace(QString::fromUtf8("！"), "!");
    pin15 = pin15.replace(QString::fromUtf8("，"), ", ");
    pin15 = pin15.replace(QString::fromUtf8("；"), "; ");
    pin15 = pin15.replace(QString::fromUtf8("："), ": ");
    pin15 = pin15.replace(QString::fromUtf8("“"), "\"");
    pin15 = pin15.replace(QString::fromUtf8("”"), "\" ");
    pin15 = pin15.replace(QString::fromUtf8("、"), ", ");
    pin15 = pin15.replace(QString::fromUtf8("（"), "(");
    pin15 = pin15.replace(QString::fromUtf8("）"), ") ");
    pin15 = pin15.replace(QString::fromUtf8("《"), "\"");
    pin15 = pin15.replace(QString::fromUtf8("》"), "\" ");
    pin15 = pin15.replace(QString::fromUtf8(" ˙"), QString::fromUtf8("˙"));
    pin15 = pin15.replace(" .", ".");
    pin15 = pin15.replace(" ?", "?");
    pin15 = pin15.replace(" !", "!");
    pin15 = pin15.replace(" ,", ",");
    pin15 = pin15.replace(" ;", ";");
    pin15 = pin15.replace(" :", ":");
    pin15 = pin15.replace("( ", "(");
    pin15 = pin15.replace(" )", ")");
    pin15 = pin15.replace("\r\n", "\n");
//    qWarning("FvChToPin: pin15_4 = " + pin15.toUtf8());

    //Többszörös szóközök kivevése:
    QString st = "";
    for (int i=0; i<pin15.length(); i++) {
        QString b = pin15.mid(i, 1);
        if (b == " " && st.right(1) == " ") {
            //semmi
        } else {
            st = st + b;
        }
    }
    pin15 = st.trimmed();
//    qWarning("FvChToPin: pin15_3 = " + pin15.toUtf8());

    //Nagybetűk elé szóköz:
    st = pin15.left(1);
    for (int i=1; i<pin15.length(); i++) {
        QString b = pin15.mid(i, 1);
        if (pin15.mid(i-1, 1) != " " && pin15.mid(i-1, 1) != "\n" &&
                pin15.mid(i-1, 1) != "&" && b.toLower() != b) {
            st = st + " " + b;
        } else {
            st = st + b;
        }
    }
    pin15 = st;

    return pin15;
}

QString Szotar::getTone(QString pin15) {
    QString st = pin15.right(1);
    if (pin15.length() > 1 &&
            QRegExp("[a-zA-Z]").exactMatch(pin15.mid(pin15.length() - 2, 1))
            && QString("12345").indexOf(st) > -1) {
        return pin15.right(1);
    } else {
        return "0";
    }
}

QString Szotar::setTone(QString pin15, QString tone) {
    QString st = pin15;
    st.chop(1);
    return st + tone;
}

QString Szotar::nagybetusTone(QString pin15) {
    return pin15.left(1).toUpper() + pin15.right(pin15.length() - 1);
}

void Szotar::Pin15LKiiras(QList<QStringList> *pin15L, QString elsoSor) {
    QString kiirSt = elsoSor + "\n";
    for (int i=0; i<pin15L->size(); i++) {
        kiirSt = kiirSt + "pin15L[" + QString::number(i) + "] =";
        for (int j=0; j<pin15L->at(i).size(); j++) {
            kiirSt = kiirSt + " |" + pin15L->at(i)[j] + "|";
        }
        kiirSt = kiirSt + "\n";
    }
    qWarning(kiirSt.toLatin1());
}

void Szotar::srtKonverter(QString forrasFajlNev, QString celFajlNev) {
    //qWarning(QString::fromUtf8("Szotar::srtKonverter, forrás: ").toLatin1() + forrasFajlNev.toUtf8() +
    //        QString::fromUtf8(" , cél: ").toLatin1() + celFajlNev.toUtf8());
    QString fst = fajlToQString(forrasFajlNev); //forrás string
    QString cst = "";  //cél string
    QStringList fstl = fst.split("\n"); //forrás lista
    bool isFeliratFajl = false;

    //Tényleg feliratfájl?:
    QRegExp rx("\\d\\d\\d --> \\d\\d:");
    for (int i=0; i < qMin(fstl.size(), 10); i++) {
        if (rx.indexIn(fstl[i]) != -1) { //van idősor
            isFeliratFajl = true;
            break;
        }
    }

    bool isNagyKezdoBetu = true;
    if (isFeliratFajl) {    //felirat fájl:
        QString idoSor;
        QString chst = "";
        int n = 0;
        for (int i=0; i<fstl.size(); i++) {
            QString sorSt = fstl[i].trimmed();
            if (i < fstl.size()-1 && rx.indexIn(fstl[i+1]) != -1) { //idősor a következő:
                if (QRegExp("\\d+").indexIn(sorSt) > -1 || sorSt == "") {    //sorszám v. üres sor
                    ;   //semmi
                } else {
                    chst = chst + sorSt;
                }
                if (n > 0) {    //aktuális adag kiírása:
                    cst = cst + QString::number(n) + "\n";
                    cst = cst + idoSor + "\n";
                    QString pinSt = FvChToPin(chst);
                    cst = cst + FvChToSt(chst) + "\n";
                    if (isNagyKezdoBetu) {
                        pinSt = pinSt.left(1).toUpper() + pinSt.right(pinSt.length()-1);
                    }
                    cst = cst + pinSt + "\n";
                    cst = cst + "\n";
                    //qWarning("cst: " + cst.toUtf8());
                    isNagyKezdoBetu = (chst.right(1).indexOf(QString::fromUtf8("。？！")) > -1);
                }
                w->ui->bLabel5->setText(idoSor + QString::fromUtf8(" kész."));
                n++;
                idoSor = fstl[i+1];
                i++;
                chst = "";
            } else {    //nem idősor a következő:
                if (sorSt != "") {
                    if (chst == "") {
                        chst = FvChToSt(sorSt);
                    } else {
                        chst = chst + " " + FvChToSt(sorSt);
                    }
                }
            }
        }
        if (n > 0) {    //utolsó adag kiírása:
            cst = cst + QString::number(n) + "\n";
            cst = cst + idoSor + "\n";
            cst = cst + chst + "\n";
            QString pinSt = FvChToPin(chst);
            if (isNagyKezdoBetu) {
                pinSt = pinSt.left(1).toUpper() + pinSt.right(pinSt.length()-1);
            }
            cst = cst + pinSt + "\n";
        }
    } else {    //nem felirat fájl:
        for (int i=0; i<fstl.size(); i++) {
            QString sorSt = fstl[i].trimmed();
            if ( sorSt != "" || i<fstl.size()-1 ) {
                QString pinSt = FvChToPin(sorSt);
                if (isNagyKezdoBetu) {
                    pinSt = pinSt.left(1).toUpper() + pinSt.right(pinSt.length()-1);
                }
                isNagyKezdoBetu = (sorSt.right(1).indexOf(QString::fromUtf8("。？！")) > -1);
                if (cst == "") {
                    cst = FvChToSt(sorSt) + "\n" + pinSt + "\n";

                } else {
                    cst = cst + "\n" + FvChToSt(sorSt) + "\n" + pinSt + "\n";

                }
            }
        }
    }
    QStToFajl(celFajlNev, cst, true);
}

QString Szotar::rebToLat(QString reb) {
    QString reb2 = "";
    while(reb != "") {
        long idx = syllabaryL.keresSzotar(reb);
        if (idx == -1) {
            reb2 += reb.left(1);
            reb.remove(0, 1);
        } else {
            int l = syllabaryL.getKey(idx).size();
            reb2 += syllabaryL.getValue(idx);
            reb.remove(0, l);
        }
    }
    return reb2;
}

void Szotar::jpSzotBerak(QString keb, QString reb, QString sense) {
    long idx = jpListL.keresBeszur(keb);
    if (idx < jpListL.size() && keb == jpListL.getKey(idx)) {    //nem új szó:
        long idx2 = jpListL.getValue(idx);
        jpNyersL[idx2] += SZEPA1 + reb + SZEPA2 + sense + SZEPA3;
    } else {
        jpNyersL << SZEPA1 + reb + SZEPA2 + sense + SZEPA3;
        jpListL.beszur(idx, keb, jpListL.size());
    }
}

QString Szotar::FvJpToLat(QString kerdez0) {
    QRegExp reg(SZEPA2 + ".*" + SZEPA3);
    reg.setMinimal(true);
    QString kerdez = kerdez0;
    QString reb = "";
    while (kerdez != "") {
        long idx = jpListL.keresSzotar(kerdez);
        if (idx == -1) {
            reb += " " + kerdez.mid(0, 1) + " ";
            kerdez = kerdez.mid(1);
        } else {
            QString st = jpNyersL[jpListL.getValue(idx)];
            st = st.replace(reg, "").replace(SZEPA1, "&").mid(1);
            reb += st;
            kerdez = kerdez.mid(jpListL.getKey(idx).length());
        }
    }
    reb.replace("  ", " ");
    reb = reb.trimmed();
    return reb;
}

QString Szotar::FvJpToEnNyers(QString kerdez0) {
    //kimenet: jp + SZEPA6 + kiejtés + SZEPA6 + jelentés + "\n"
    QRegExp reg(SZEPAENT_SEC + "(.*)" + SZEPA3);
    reg.setMinimal(true);
    QString kerdez = kerdez0;
    QString er = "";
    while (kerdez != "") {
        long idx = jpListL.keresSzotar(kerdez);
        if (idx == -1) {
            QString  st = kerdez.left(1);
            er += st + SZEPA6 + SZEPA6 + "\n";
            kerdez = kerdez.mid(1);
        } else {
            er += jpListL.getKey(idx) + jpNyersL[jpListL.getValue(idx)] + "\n";
            kerdez = kerdez.mid(jpListL.getKey(idx).length());
        }
    }
    er = er.replace(reg, "\n");
    er = er.replace(SZEPA1, SZEPA6);
    er = er.replace(SZEPA2, SZEPA6 + "/");
    er.chop(1);
    return er;
}

QString Szotar::FvJpToEn(QString kerdez0) {
    //kimenet: jp + SZEPA6 + kiejtés + SZEPA6 + jelentés + >ent_seq< + "\n"
    QRegExp reg(SZEPAENT_SEC + "(.*)" + SZEPA3);
    reg.setMinimal(true);
    QString kerdez = kerdez0;
    QString er = "";
    while (kerdez != "") {
        long idx = jpListL.keresSzotar(kerdez);
        if (idx == -1) {
            QString  st = kerdez.left(1);
            er += st + SZEPA6 + SZEPA6 + ".\n";
            kerdez = kerdez.mid(1);
        } else {
            er += jpListL.getKey(idx) + jpNyersL[jpListL.getValue(idx)] + ".\n";
            kerdez = kerdez.mid(jpListL.getKey(idx).length());
        }
    }
    er = er.replace(reg, " &gt;\\1&lt;");
    er = er.replace(SZEPA1, SZEPA6);
    er = er.replace(SZEPA2, SZEPA6);
    er = er.replace(SZEPA3, "");
    er.chop(1);
    return er;
}
