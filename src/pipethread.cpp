﻿#include <windows.h>
#include <QString>
#include "pipethread.h"
#include "Szotar.h"
#include "mainwindow.h"

extern bool pipeThreadVege;
extern LPTSTR QStringToLPTSTR(QString lineSt);
extern QString LPTSTRToQString(LPTSTR lineSt);
extern MainWindow *w;
extern Szotar *sz;

extern int maxKarakterSzam;
//DWORD WINAPI StartPipe2(LPVOID lpvParam);


PipeThread::PipeThread(QObject *parent) :
    QThread(parent)
{
}

void GetAnswerToRequest(LPTSTR pchRequest, LPTSTR pchReply, LPDWORD pchBytes)
{

    w->konzolOut(w->satr("Kliens lekérdezés") + ": {0}\n", pchRequest, false);
    QString kerdes = LPTSTRToQString(pchRequest);

    QString valasz = sz->ValaszFv(kerdes);  //itt kérdezünk

    LPTSTR stV;
    //qWarning("st.length() = %d", st.length());
    if (valasz.length() > maxKarakterSzam) {
        valasz = w->satr("_HIBA: Válasz karakterszám") + " = " + QString::number(valasz.length())
                + " > " + w->satr("Max karakterszám egy lekérdezésben") + " = " +
                QString::number(maxKarakterSzam)
                + ". " + w->satr("Rövidebb szöveget adj meg!");
        stV = QStringToLPTSTR(valasz);
    } else if (valasz == "-") {
        stV = L"-";
    } else {
        stV = QStringToLPTSTR(valasz);
    }
    wcscpy(pchReply, stV);
    *pchBytes = (wcslen(stV) + 1) * sizeof(WCHAR);
}

DWORD WINAPI InstanceThread(LPVOID lpvParam)
{
    HANDLE hHeap = GetProcessHeap();
    WCHAR* pchRequest = (WCHAR*)HeapAlloc(hHeap, 0, (maxKarakterSzam+1) * sizeof(WCHAR));
    WCHAR* pchReply = (WCHAR*)HeapAlloc(hHeap, 0, (maxKarakterSzam+1) * sizeof(WCHAR));

    DWORD cbBytesRead = 0, cbReplyBytes = 0, cbWritten = 0;
    BOOL fSuccess = FALSE;
    HANDLE hPipe = NULL;

    if (lpvParam == NULL) {
        w->konzolOut("\n" +
                w->satr("_HIBA_1 - nem várt NULL érték lpvParam -ban, pipe bezárva.") +
                "\n", true);
        if (pchReply != NULL) HeapFree(hHeap, 0, pchReply);
        if (pchRequest != NULL) HeapFree(hHeap, 0, pchRequest);
        return (DWORD)-1;
    }

    if (pchRequest == NULL) {
        w->konzolOut("\n" +
        w->satr("_HIBA_2 - nem várt NULL érték pchRequest -ben, pipe bezárva.") +
        "\n", true);
        if (pchReply != NULL)
            HeapFree(hHeap, 0, pchReply);
        return (DWORD)-1;
    }

    if (pchReply == NULL) {
        w->konzolOut("\n" +
                w->satr("_HIBA_3 - nem várt NULL érték pchReply -ban, pipe bezárva.") +
                "\n", true);
        if (pchRequest != NULL)
            HeapFree(hHeap, 0, pchRequest);
        return (DWORD)-1;
    }
    w->konzolOut(w->satr("Pipe megnyitva, kliens lekérdezések feldolgozása folyamatban.") +
            "\n", false);
    hPipe = (HANDLE)lpvParam;
    while (!pipeThreadVege) {
        // Read client requests from the pipe. This simplistic code only allows messages
        // up to BUFSIZE characters in length.
        fSuccess = ReadFile(
        hPipe,        // handle to pipe
        pchRequest,    // buffer to receive data
        maxKarakterSzam * sizeof(WCHAR), // size of buffer
        &cbBytesRead, // number of bytes read
        NULL);        // not overlapped I/O

        if (!fSuccess || cbBytesRead == 0) 		{
            if (GetLastError() == ERROR_BROKEN_PIPE) {
                w->konzolOut(w->satr("Kliens kilépett") + ", GLE={0}.\n",	GetLastError(), false);
            } else {
                w->konzolOut(w->satr("Kliens lekérdezés beolvasás sikertelen") + ", GLE={0}.\n", GetLastError(), true);
            }
            break;
        }

        // Process the incoming message.
        GetAnswerToRequest(pchRequest, pchReply, &cbReplyBytes);

        // Write the reply to the pipe.
        fSuccess = WriteFile(
        hPipe,        // handle to pipe
        pchReply,     // buffer to write from
        cbReplyBytes, // number of bytes to write
        &cbWritten,   // number of bytes written
        NULL);        // not overlapped I/O

        if (!fSuccess || cbReplyBytes != cbWritten)
        {
            w->konzolOut(w->satr("Szerver válaszküldés sikertelen") +
                    ", GLE={0}.\n", GetLastError(), true);
            break;
        }
    }

    FlushFileBuffers(hPipe);
    DisconnectNamedPipe(hPipe);
    CloseHandle(hPipe);

    HeapFree(hHeap, 0, pchRequest);
    HeapFree(hHeap, 0, pchReply);

    w->konzolOut(w->satr("Pipe bezárás.") + "\n", false);
    return 1;
}

void PipeThread::StartPipe()
{
    BOOL   fConnected = FALSE;
    DWORD  dwThreadId = 0;
    HANDLE hPipe = INVALID_HANDLE_VALUE, hThread = NULL;
    QString pipeNev = w->getPipeNev();
    LPTSTR lpszPipename = (LPTSTR)pipeNev.utf16();
    w->konzolOut(w->satr("Max karakterszám egy lekérdezésben") + ": {0}", maxKarakterSzam, true);

    for (;;) {
        w->konzolOut(w->satr("Pipe szerver: várakozás a kliens kapcsolódásra ezen a pipe-on") +
                ": {0}\n", lpszPipename, false);

        hPipe = CreateNamedPipe(
            lpszPipename,             // pipe name
            PIPE_ACCESS_DUPLEX,       // read/write access
            PIPE_TYPE_MESSAGE |       // message type pipe
            PIPE_READMODE_MESSAGE |   // message-read mode
            PIPE_WAIT,                // blocking mode
            PIPE_UNLIMITED_INSTANCES, // max. instances
            maxKarakterSzam * sizeof(WCHAR),                  // output buffer size
            maxKarakterSzam * sizeof(WCHAR),                  // input buffer size
            0,                        // client time-out
            NULL);                    // default security attribute

        if (hPipe == INVALID_HANDLE_VALUE) {
            w->konzolOut(w->satr("Pipe létrehozása sikertelen") + " (1), GLE = {0}.\n"
                    , GetLastError(), true);
            return;
        }

        fConnected = ConnectNamedPipe(hPipe, NULL) ? TRUE : (GetLastError() == ERROR_PIPE_CONNECTED);

        if (fConnected) {
            w->konzolOut(w->satr("Kliens kapcsolódott, lekérdezés feldolgozás indul.") +
                     "\n", false);

            //InstanceThread(hPipe);

            // Create a thread for this client.
            hThread = CreateThread(
            NULL,              // no security attribute
            0,                 // default stack size
            InstanceThread,    // thread proc
            (LPVOID)hPipe,    // thread parameter
            0,                 // not suspended
            &dwThreadId);      // returns thread ID

            if (hThread == NULL) {
                w->konzolOut(w->satr("Pipe létrehozása sikertelen") +
                        " (2), GLE={0}.\n", GetLastError(), true);
                return;
            } else {
                CloseHandle(hThread);
            }

        } else {
            // The client could not connect, so close the pipe.
            CloseHandle(hPipe);
        }
    }
    return;
}

void PipeThread::run() {
    this->StartPipe();
}
