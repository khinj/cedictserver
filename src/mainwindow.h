﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <QPlainTextEdit>
#include <QSystemTrayIcon>
#include <QColorDialog>
#include <QFontDialog>
#include <QSignalMapper>
#include <QScrollBar>
#include <windows.h>
#include "satextedit.h"
#include "ui_mainwindow.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void closeEvent(QCloseEvent *event);
    void konzolOut(QString lineSt, bool isForced);
    void konzolOut(QString lineSt, QString par, bool isForced);
    void konzolOut(QString lineSt, LPTSTR par, bool isForced);
    void konzolOut(QString lineSt, DWORD par, bool isForced);
    QString getPipeNev();
    Ui::MainWindow *ui;
    void localSzotarBeolvasasKesz2();
    bool foglalt;
    void wheelEvent(QWheelEvent *e);
    void mousePressEvent (QMouseEvent *e);
    void keyPressEvent(QKeyEvent *e);
    QWidget *egerAlatt(QPoint p);
    QWidget *mFr;
    void bTextEditSetPlainText(QPlainTextEdit *te, QString st);
    void hideEvent(QHideEvent *);
    QSystemTrayIcon *trayIcon;
    QColorDialog *colorDialog;
    QFontDialog *fontDialog;
    QString nyelv;  //aktuáluis nyelv, pl hun, eng
    QString nyelvSt;    //aktuális nyelvi fordító file
    QString satr(QByteArray ba);
    QMenu *nyelvMenu;
    QMenu *nyelvMenu2;
    QMenu *tab2NyelvMenu;
    QPalette bMentButtonPalette;

    void nyelvBeallitas();
    SaTextEdit *kerdezTextEdit;
    SaTextEdit *valaszTextEdit;
    SaTextEdit *pipeTextEdit;
    bool event(QEvent *e);
    QString valaszTextEditStilus, valaszTextEditStilusCh,
            valaszTextEditStilusPin, pipeTextEditStilus;
    void helyiValasztKiir(QString kerdezSt);

public slots:
    void appendSlot(QString lineSt, bool isForced);
    void helyiValasz();
    void helyiValasz(int dummy);
    void helyiValaszSelected();
    void kellBeallitasMentes();
    void beallitasMentes();
    void bTextEditSetPlainTextSlot(QPlainTextEdit *te, QString st);
    void bMentButtonSzinVisszaallitas();
    void srtKonverter();
    void talloz1();
    void talloz2();
    void talloz3();
    void talloz4();
    void celSrtFajlNevBeallit();
    void celSrtFajlNevEll();
    void pipeKijelzesTorles();
    void trayIconClicked(QSystemTrayIcon::ActivationReason);
    void showContextMenu(const QPoint &pt);
    void showBetuszinDialog();
    void showBetuszinDialogCh();
    void showBetuszinDialogPin();
    void showHatterszinDialog();
    void showFontDialog();
    void showFontDialogCh();
    void showFontDialogPin();
    void setBetuSzin(QColor col);
    void setBetuSzinCh(QColor col);
    void setBetuSzinPin(QColor col);
    void setHatterSzin(QColor col);
    void setSaFont(QFont font);
    void setSaFontCh(QFont font);
    void setSaFontPin(QFont font);
    void nyelvValtas(QString);
    void szoListVisszaUgras();
    void szoListEloreUgras();
    void cedictKeres();

signals:
    void appendSignal(QString lineSt, bool isForced);
    void localSzotarBeolvasasKesz();
    void bTextEditSetPlainTextSignal(QPlainTextEdit *te, QString st);
    void trayIconSignal(bool visible);
    void nyelvSignal(QString);

private:
};

#endif // MAINWINDOW_H
