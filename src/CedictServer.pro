#https://stackoverflow.com/questions/2580934/how-to-specify-different-debug-release-output-directories-in-qmake-pro-file

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TEMPLATE = app

Debug:TARGET = ../../bin/debug/CedictServer
Release:TARGET = ../../bin/release/CedictServer

DEFINES += UNICODE

SOURCES += main.cpp \
    mainwindow.cpp \
    pipethread.cpp \
    Szotar.cpp \
    MyList.cpp \
    satextedit.cpp \
    tcpipserver.cpp \
    tcpipsocket.cpp

HEADERS  += mainwindow.h \
    pipethread.h \
    Szotar.h \
    main.h \
    MyList.h \
    satextedit.h \
    tcpipserver.h \
    tcpipsocket.h


FORMS    += mainwindow.ui

RC_FILE = CedictServer.rc

OTHER_FILES += \
    CedictServer.rc
