﻿#include <QFileDialog>
#include <QMenu>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "Szotar.h"

extern Szotar *sz;
extern QString iniFajlSt;
extern QString iniFajlNev;
extern QString exePath;
extern QString LPTSTRToQString(LPTSTR lineSt);
extern void setParameter(QString *forras, QString parNev, QString ertek);
extern QString getParameter(QString *forras, QString parNev, QString defaultErtek = NULL);
extern void QStToFajl(QString fajlNev, QString st, bool kellBOM);
extern void kilepes();
extern QApplication *a;
extern QString fajlToQString(QString fajlNev);
extern QString SZEPA1;
extern QString SZEPA3;
extern QString SZEPA6;
extern QString PINSZOKOZ;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    kerdezTextEdit = new SaTextEdit(this->ui->splitter);
    valaszTextEdit = new SaTextEdit(this->ui->splitter);
    pipeTextEdit = new SaTextEdit(this->ui->splitter_2);

    kerdezTextEdit->setObjectName(QString::fromUtf8("kerdezTextEdit"));
    valaszTextEdit->setObjectName(QString::fromUtf8("valaszTextEdit"));
    pipeTextEdit->setObjectName(QString::fromUtf8("pipeTextEdit"));
    kerdezTextEdit->setFocusPolicy(Qt::StrongFocus);
    kerdezTextEdit->setTabChangesFocus(false);
    kerdezTextEdit->setAcceptDrops(false);
    kerdezTextEdit->setAutoFillBackground(true);
    valaszTextEdit->setParent(this->ui->splitter);
    valaszTextEdit->setAcceptDrops(false);
    valaszTextEdit->setAutoFillBackground(true);
    pipeTextEdit->setParent(this->ui->splitter_2);
    pipeTextEdit->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    QList<int> iList;
    iList << 50 << 1000;
    this->ui->splitter_2->setSizes(iList);

    QObject::connect(this, SIGNAL(appendSignal(QString, bool)),
                         this, SLOT(appendSlot(QString, bool)));
    QObject::connect(kerdezTextEdit, SIGNAL(textChanged()),
                         this, SLOT(helyiValasz()));
    QObject::connect(ui->comboBox, SIGNAL(currentIndexChanged(int)),
                         this, SLOT(helyiValasz(int)));
    QObject::connect(ui->checkBox_2, SIGNAL(stateChanged(int)),
                         this, SLOT(helyiValasz(int)));
    QObject::connect(ui->checkBox_4, SIGNAL(stateChanged(int)),
                         this, SLOT(helyiValasz(int)));
    QObject::connect(this, SIGNAL(localSzotarBeolvasasKesz()),
                         this, SLOT(helyiValasz()));
    QObject::connect(ui->bTextEdit1, SIGNAL(textChanged()),
                         this, SLOT(kellBeallitasMentes()));
    QObject::connect(ui->bTextEdit1_2, SIGNAL(textChanged()),
                         this, SLOT(kellBeallitasMentes()));
    QObject::connect(ui->bTextEdit2, SIGNAL(textChanged()),
                         this, SLOT(kellBeallitasMentes()));
    QObject::connect(ui->bTextEdit3, SIGNAL(textChanged()),
                         this, SLOT(celSrtFajlNevBeallit()));
    QObject::connect(ui->bTextEdit4, SIGNAL(textChanged()),
                         this, SLOT(celSrtFajlNevEll()));
    QObject::connect(this, SIGNAL(bTextEditSetPlainTextSignal(QPlainTextEdit*, QString)),
                         this, SLOT(bTextEditSetPlainTextSlot(QPlainTextEdit*, QString)));
    QObject::connect(ui->bMentButton, SIGNAL(clicked()),
                         this, SLOT(beallitasMentes()));
    QObject::connect(ui->bMegseButton, SIGNAL(clicked()),
                         this, SLOT(bMentButtonSzinVisszaallitas()));
    QObject::connect(ui->bKonvertalButton, SIGNAL(clicked()),
                         this, SLOT(srtKonverter()));
    QObject::connect(ui->bButton1, SIGNAL(clicked()),
                         this, SLOT(talloz1()));
    QObject::connect(ui->bButton2, SIGNAL(clicked()),
                         this, SLOT(talloz2()));
    QObject::connect(ui->bButton3, SIGNAL(clicked()),
                         this, SLOT(talloz3()));
    QObject::connect(ui->bButton4, SIGNAL(clicked()),
                         this, SLOT(talloz4()));
    QObject::connect(ui->kijelzesTorlesButton, SIGNAL(clicked()),
                         this, SLOT(pipeKijelzesTorles()));
    QObject::connect(this->kerdezTextEdit,
            SIGNAL(selectionChanged()), this, SLOT(helyiValaszSelected()));

    valaszTextEdit->setReadOnly(true);
    pipeTextEdit->setReadOnly(true);
    ui->comboBox->insertItem(0,"Fv1Nyers");
    ui->comboBox->insertItem(1,"Fv1NyersB");
    ui->comboBox->insertItem(2,"FvMNyers");
    ui->comboBox->insertItem(3,"FvMNyersB");
    ui->comboBox->insertItem(4,"FvChToEnNyers");
    ui->comboBox->insertItem(5,"FvChToEn");
    ui->comboBox->insertItem(6,"FvChToPin");
    ui->comboBox->insertItem(7,"FvPinToPin15");
    ui->comboBox->insertItem(8,"FvPin15ToPin");
    ui->comboBox->insertItem(9,"FvChToSt");
    ui->comboBox->insertItem(10,"FvChToTr");
    ui->comboBox->insertItem(11,"FvJpToLat");
    ui->comboBox->insertItem(12,"FvJpToEnNyers");
    ui->comboBox->insertItem(13,"FvJpToEn");
    ui->comboBox->insertItem(14,"TESZT");
    ui->comboBox->insertItem(15,"CMD");
    kerdezTextEdit->setFocus();
    foglalt = false;
    trayIcon = new QSystemTrayIcon(QIcon(exePath + "CS.png"), this);
    trayIcon->setToolTip("CedictServer");
    QObject::connect(this, SIGNAL(trayIconSignal(bool)),
                         trayIcon, SLOT(setVisible(bool)));
    QObject::connect(trayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)),
                         this,
                         SLOT(trayIconClicked(QSystemTrayIcon::ActivationReason)));
    fontDialog = new QFontDialog;
    fontDialog->setModal(true);
    colorDialog = new QColorDialog;
    colorDialog->setModal(true);
    connect(this, SIGNAL(nyelvSignal(QString)), this, SLOT(nyelvValtas(QString)));
    nyelvMenu2 = new QMenu;
    tab2NyelvMenu = new QMenu;
    tab2NyelvMenu->addMenu(nyelvMenu2);
    ui->horizontalLayout_3->addWidget(tab2NyelvMenu);
    tab2NyelvMenu->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Expanding);
//    tab2NyelvMenu->setMinimumHeight(20);
    QPalette pal = tab2NyelvMenu->palette();
    pal.setColor(QPalette::Window, QColor("lightgray"));
    tab2NyelvMenu->setPalette(pal);
    tab2NyelvMenu->setAutoFillBackground(true);
    QFont font;
    font = tab2NyelvMenu->font();
    font.setPointSize(10);
    tab2NyelvMenu->setFont(font);
    font = nyelvMenu2->font();
    font.setPointSize(12);
    nyelvMenu2->setFont(font);
    nyelvMenu2->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Minimum);
    tab2NyelvMenu->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Minimum);
    connect(tab2NyelvMenu, SIGNAL(aboutToHide()), tab2NyelvMenu, SLOT(show()));
    bMentButtonPalette = ui->bMentButton->palette();
}


MainWindow::~MainWindow() {
    delete(colorDialog);
    delete(fontDialog);
    delete(tab2NyelvMenu);
    delete(kerdezTextEdit);
    delete(valaszTextEdit);
    delete(pipeTextEdit);
}


void MainWindow::closeEvent(QCloseEvent *event) {
    foglalt = true; //azért, hogy a hideEvent ne fusson le
    kilepes();
    trayIcon->hide();
    delete trayIcon;
    delete ui;
    QMainWindow::closeEvent(event);
}

void MainWindow::konzolOut(QString st, bool isForced) {
    emit appendSignal(st, isForced);
}

void MainWindow::konzolOut(QString st, QString par, bool isForced) {
    QString st1 = par;
    QString st2 = st;
    st2.replace("{0}", st1);
    emit appendSignal(st2, isForced);
}

void MainWindow::konzolOut(QString st, LPTSTR par, bool isForced) {
    QString st1 = LPTSTRToQString(par);
    QString st2 = st;
    st2.replace("{0}", st1);
    emit appendSignal(st2, isForced);
}

void MainWindow::konzolOut(QString st, DWORD par, bool isForced) {
    QString st1 = QString::number(par);
    QString st2 = st;
    st2.replace("{0}", st1);
    emit appendSignal(st2, isForced);
}

QString MainWindow::getPipeNev() {
    return getParameter(&iniFajlSt, "pipeNev");
}

void MainWindow::appendSlot(QString st, bool isForced) {
    if (isForced || ui->checkBox->isChecked()) {
        pipeTextEdit->append(">> " + st);
    }
}

void MainWindow::helyiValaszSelected() {
    if (!foglalt && sz) {
        QString st = "";
        if (kerdezTextEdit->selList.size() > 0) {
            QList<QTextEdit::ExtraSelection> extraSelections;
            for (int i=0; i<kerdezTextEdit->selList.size(); i++) {
                QTextEdit::ExtraSelection selection;
                selection.cursor = kerdezTextEdit->textCursor();
                selection.format.setBackground(kerdezTextEdit->betuSzin);
                selection.format.setForeground(kerdezTextEdit->hatterSzin);
                selection.cursor.setPosition(kerdezTextEdit->selList[i].x(), QTextCursor::MoveAnchor);
                selection.cursor.setPosition(kerdezTextEdit->selList[i].y(), QTextCursor::KeepAnchor);
                extraSelections.append(selection);
            }
            kerdezTextEdit->setExtraSelections(extraSelections);
            QTextCursor cur = kerdezTextEdit->textCursor();
            cur.setPosition(kerdezTextEdit->selList.last().y(), QTextCursor::MoveAnchor);
            kerdezTextEdit->setTextCursor(cur);
            for(int i=0; i<kerdezTextEdit->extraSelections().size(); i++) {
                if (i > 0 && ui && ui->checkBox_5) {
                    st = st + (ui->checkBox_5->isChecked() ? SZEPA1 : "");
                }
                st = st + kerdezTextEdit->toPlainText().mid(
                        kerdezTextEdit->selList[i].x(),
                        kerdezTextEdit->selList[i].y() - kerdezTextEdit->selList[i].x());
            }
        } else {
            QList<QTextEdit::ExtraSelection> extraSelections;
            kerdezTextEdit->setExtraSelections(extraSelections);
            st = kerdezTextEdit->textCursor().selectedText();
        }
        if (st != "") {
            foglalt = true;
            helyiValasztKiir(st);
            foglalt = false;
        } else {
            helyiValasz();
        }
    }
}

void MainWindow::helyiValasz(int dummy) {
    kerdezTextEdit->selTorol();
    kerdezTextEdit->textCursor().setPosition(
        kerdezTextEdit->toPlainText().length() / 2);
    helyiValasz();
}

void MainWindow::helyiValasz() {
    if (!foglalt && sz) {
        foglalt = true;
        if (kerdezTextEdit->toPlainText() != "") {
            if (sz->szoListIdx == -1 ||
                    kerdezTextEdit->toPlainText().indexOf(
                    sz->szoList[sz->szoListIdx]) == -1 ) {
                while (sz->szoListIdx + 1 < sz->szoList.size()) {
                    sz->szoList.removeLast();
                }
                sz->szoList.append(kerdezTextEdit->toPlainText());
                sz->szoListIdx++;
                if (sz->szoList.size() > 100) {
                    sz->szoList.removeFirst();
                    sz->szoListIdx--;
                }
            } else {
                sz->szoList[sz->szoListIdx] = kerdezTextEdit->toPlainText();
            }
        }
        int sbv = valaszTextEdit->verticalScrollBar()->value();
        helyiValasztKiir(kerdezTextEdit->toPlainText());
        valaszTextEdit->verticalScrollBar()->setValue(sbv);
        ui->szoListLabel->setText(". " + QString::number(sz->szoListIdx + 1) + " / " +
                QString::number(sz->szoList.size()) + " .");
        foglalt = false;
    }

}

void MainWindow::localSzotarBeolvasasKesz2() {
    emit(this->localSzotarBeolvasasKesz());
}

void MainWindow::wheelEvent(QWheelEvent *e) {
    //qWarning("h1: " + egerAlatt(e->globalPos())->objectName().toLatin1());
    if ((e->modifiers() & Qt::ControlModifier) &&
            egerAlatt(e->globalPos())->objectName() == "kerdezTextEdit") {
        QFont font;
        font = kerdezTextEdit->font();
        int size = font.pointSize();
        if (e->delta() == -120) {
            size--;
        } else if (e->delta() == 120) {
            size++;
        }
        font.setPointSize(size);
        kerdezTextEdit->setFont(font);
    } else if ((e->modifiers() & Qt::ControlModifier) &&
            egerAlatt(e->globalPos())->objectName() == "valaszTextEdit") {
        QFont font;
        font = valaszTextEdit->font();
        int size0 = font.pointSize();
        int size = font.pointSize();
        if (e->delta() == -120) {
            size--;
        } else if (e->delta() == 120) {
            size++;
        }
        font.setPointSize(size);
        valaszTextEdit->setFont(font);
        int sizeD = font.pointSize() - size0;
        QStringList stL;
        stL = valaszTextEditStilus.split("|");
        valaszTextEditStilus =
            stL[0] + "|" +
            stL[1] + "|" +
            stL[2] + "|" +
            stL[3] + "|" +
            stL[4] + "|" +
            stL[5] + "|" +
            stL[6] + "|" +
            QString::number((stL[7]).toInt() + sizeD) + "|" +
            stL[8];
        stL = valaszTextEditStilusCh.split("|");
        valaszTextEditStilusCh =
            stL[0] + "|" +
            stL[1] + "|" +
            stL[2] + "|" +
            stL[3] + "|" +
            QString::number((stL[4]).toInt() + sizeD) + "|" +
            stL[5];
        stL = valaszTextEditStilusPin.split("|");
        valaszTextEditStilusPin =
            stL[0] + "|" +
            stL[1] + "|" +
            stL[2] + "|" +
            stL[3] + "|" +
            QString::number((stL[4]).toInt() + sizeD) + "|" +
            stL[5];
        QString st = kerdezTextEdit->textCursor().selectedText();
        if (st == "") {
            st = kerdezTextEdit->toPlainText();
        }
        helyiValasztKiir(st);
    } else if ((e->modifiers() & Qt::ControlModifier) &&
            egerAlatt(e->globalPos())->objectName() == "pipeTextEdit") {
        QFont font;
        font = pipeTextEdit->font();
        int size = font.pointSize();
        if (e->delta() == -120) {
            size--;
        } else if (e->delta() == 120) {
            size++;
        }
        font.setPointSize(size);
        pipeTextEdit->setFont(font);
    } else {
        QMainWindow::wheelEvent(e);
    }
}

void MainWindow::keyPressEvent(QKeyEvent *e) {
//    qWarning("MainWindow::keyPressEvent");
    if (e->modifiers() == Qt::ControlModifier && e->key() == Qt::Key_PageUp) {   //Ctrl + PgUp
        int idx = ui->tabWidget->currentIndex() - 1;
        if (idx < 0) {
            idx = ui->tabWidget->count() - 1;
        }
        ui->tabWidget->setCurrentIndex(idx);
    } else if (e->modifiers() == Qt::ControlModifier && e->key() == Qt::Key_PageDown) {   //Ctrl + PgDn
        int idx = ui->tabWidget->currentIndex() + 1;
        if (idx >= ui->tabWidget->count()) {
            idx =0;
        }
        ui->tabWidget->setCurrentIndex(idx);
    } else if (ui->tabWidget->currentIndex() == 0 &&
            e->modifiers() == Qt::KeypadModifier &&
            (e->key() == Qt::Key_4 || e->key() == Qt::Key_5 ||
            e->key() == Qt::Key_6 || e->key() == Qt::Key_9)) {
        kerdezTextEdit->setFocus();
    } else if (ui->tabWidget->currentIndex() == 0 &&
            e->modifiers() == Qt::AltModifier &&
            (e->key() == Qt::Key_Left)) {
        szoListVisszaUgras();
    } else if (ui->tabWidget->currentIndex() == 0 &&
            e->modifiers() == Qt::AltModifier &&
            (e->key() == Qt::Key_Right)) {
        szoListEloreUgras();
    } else {
        QMainWindow::keyPressEvent(e);
    }
}

void MainWindow::kellBeallitasMentes() {
    //qWarning("MainWindow::kellBeallitasMentes()");
    if (sz) {
        QString st1 = ui->bTextEdit1->toPlainText();
        QString st1_2 = ui->bTextEdit1_2->toPlainText();
        QString st2 = ui->bTextEdit2->toPlainText();
        if (st1 != sz->eredetiCedictSzotarFajlNev ||
                st1_2 != sz->eredetiJMdictSzotarFajlNev ||
                st2 != sz->eredetiLocalSzotarFajlNev) {
            ui->bMentButton->setStyleSheet("background-color: rgb(255, 0, 0); color: rgb(96, 0, 0)");
        } else {
            ui->bMentButton->setStyleSheet("");
            ui->bMentButton->setPalette(bMentButtonPalette);
        }
        QFont f = ui->bMentButton->font();
        f.setBold(true);
        ui->bMentButton->setFont(f);
    }
}

void MainWindow::beallitasMentes() {
//    qWarning("MainWindow::beallitasMentes(), cM = " + ui->bMentButton->styleSheet().toUtf8());
//    qWarning("MainWindow::beallitasMentes(), c1 = " + ui->bButton1->styleSheet().toUtf8());
    if (ui->bMentButton->styleSheet() !=
            ui->bButton1->styleSheet()) {
        setCursor(Qt::WaitCursor);

        if (sz->szotarBeolvas()) {
        } else {
            bMentButtonSzinVisszaallitas();
        }
        if (sz->localSzotarBeolvas()) {
            setParameter(&iniFajlSt, QString::fromUtf8("SAJÁT_SZÓTÁRFÁJL"), ui->bTextEdit2->toPlainText());
            sz->eredetiLocalSzotarFajlNev = ui->bTextEdit2->toPlainText();
            QStToFajl(iniFajlNev, iniFajlSt, false);
        } else {
            bMentButtonSzinVisszaallitas();
        }

        ui->bMentButton->setStyleSheet("");
        ui->bMentButton->setPalette(bMentButtonPalette);
        QFont f = ui->bMentButton->font();
        f.setBold(true);
        ui->bMentButton->setFont(f);
        setCursor(Qt::ArrowCursor);
    }
}

void MainWindow::bTextEditSetPlainText(QPlainTextEdit *te, QString st) {
    emit bTextEditSetPlainTextSignal(te, st);
}

void MainWindow::bTextEditSetPlainTextSlot(QPlainTextEdit *te, QString st) {
    te->setPlainText(st);
}

void MainWindow::bMentButtonSzinVisszaallitas() {
    ui->bMentButton->setStyleSheet("");
    ui->bMentButton->setPalette(bMentButtonPalette);
    QFont f = ui->bMentButton->font();
    f.setBold(true);
    ui->bMentButton->setFont(f);
    bTextEditSetPlainText(ui->bTextEdit1, sz->eredetiCedictSzotarFajlNev);
    bTextEditSetPlainText(ui->bTextEdit1_2, sz->eredetiJMdictSzotarFajlNev);
    bTextEditSetPlainText(ui->bTextEdit2, sz->eredetiLocalSzotarFajlNev);
}

void MainWindow::srtKonverter() {
    if (ui->bLabel5->text() != satr("Konvertálás...")) {
        ui->bLabel5->setText(satr("Konvertálás..."));
        sz->srtKonverter(ui->bTextEdit3->toPlainText(), ui->bTextEdit4->toPlainText());
        ui->bLabel5->setText(satr("Konvertálás KÉSZ."));
    }
}

void MainWindow::talloz1() {
    QFileDialog fd(this);
    fd.setFileMode(QFileDialog::ExistingFile);
    QString st = fd.getOpenFileName(this, satr("CEDICT szótár fájl"),
                                                ui->bTextEdit1->toPlainText(),
                                                satr("Szótárfájl (*.u8 *.txt);;Minden (*.*)"));
    qWarning("st: " + st.toUtf8());
    if (st != "") {
        ui->bTextEdit1->setPlainText(st);
    }
}

void MainWindow::talloz2() {
    QFileDialog fd(this);
    fd.setFileMode(QFileDialog::ExistingFile);
    QString st = fd.getOpenFileName(this, satr("Saját szótár fájl"),
                                                ui->bTextEdit2->toPlainText(),
                                                satr("Szótárfájl (*.u8 *.txt);;Minden (*.*)"));
    if (st != "") {
        ui->bTextEdit2->setPlainText(st);
    }
}

void MainWindow::talloz3() {
    QFileDialog fd(this);
    fd.setFileMode(QFileDialog::ExistingFile);
    QString st = fd.getOpenFileName(this, satr("Forrás *.srt"),
                                                ui->bTextEdit3->toPlainText(),
                                                satr("Srt fájl (*.srt);;minden ( *.*)"));
    if (st != "") {
        ui->bTextEdit3->setPlainText(st);
    }
}

void MainWindow::talloz4() {
    QFileDialog fd(this);
    fd.setFileMode(QFileDialog::Directory);
    QString st = fd.getSaveFileName(this, satr("Forrás *.srt"),
                                                ui->bTextEdit4->toPlainText(),
                                                satr("Srt fájl (*.srt);;minden ( *.*)"));
    if (st != "") {
        ui->bTextEdit4->setPlainText(st);
    }
}

void MainWindow::celSrtFajlNevBeallit() {
    //qWarning("MainWindow::celSrtFajlNevBeallit()");
    QString st1 = ui->bTextEdit3->toPlainText();
    QString st2 = ui->bTextEdit4->toPlainText();
    QString dirSt = QFileInfo(st2).absolutePath();
    QString fajlNevSt = QFileInfo(st1).baseName();
    if (fajlNevSt.endsWith("_ch")) {
        fajlNevSt = fajlNevSt.left(fajlNevSt.length()-3) + "_1chp";
    } else {
        fajlNevSt = fajlNevSt + "_1chp";
    }
    QString fajlNevKitSt = QFileInfo(st1).suffix();
    ui->bTextEdit4->setPlainText(dirSt + "/" + fajlNevSt + "." + fajlNevKitSt);
    celSrtFajlNevEll();
}

void MainWindow::celSrtFajlNevEll() {
    //qWarning("MainWindow::celSrtFajlNevEll()");
    QString st = ui->bTextEdit3->toPlainText();
    if (st.indexOf(":") == -1) {
        st = exePath + ui->bTextEdit3->toPlainText();
    }
    QFileInfo check_file(st);
    if (check_file.exists() && check_file.isFile()) {
        QString st = ui->bTextEdit4->toPlainText();
        if (st.indexOf(":") == -1) {
            st = exePath + ui->bTextEdit4->toPlainText();
        }
        st = QFileInfo(st).absolutePath();
        check_file.setFile(st);
        if (check_file.exists()) {
            if (ui->bTextEdit3->toPlainText() == ui->bTextEdit4->toPlainText()) {
                ui->bLabel5->setText(satr("Ne legyen egyforma a forrás és a cél fájl!"));
                ui->bKonvertalButton->setEnabled(false);
            } else {
                ui->bLabel5->setText(satr("Konvertálás mehet!"));
                ui->bKonvertalButton->setEnabled(true);
            }
        } else {
            ui->bLabel5->setText(satr("A célfájl helye létező mappa legyen!"));
            ui->bKonvertalButton->setEnabled(false);
        }
    } else {
        ui->bLabel5->setText(satr("Létező forrásfájlt kell megadni!"));
        ui->bKonvertalButton->setEnabled(false);
    }



}

void MainWindow::pipeKijelzesTorles() {
    pipeTextEdit->clear();
}

void MainWindow::hideEvent(QHideEvent *e) {
    if (!foglalt && ui->checkBox_3->isChecked()) {
        foglalt = true;
        emit trayIconSignal(true);
        hide();

        //azért, hogy a taskbar-on ne látszódjon:
        show();
        hide();

        e->ignore();
        foglalt = false;
    }
}

void MainWindow::trayIconClicked(QSystemTrayIcon::ActivationReason reason) {
    if (reason == QSystemTrayIcon::Trigger && !foglalt) {
        foglalt = true;
        emit trayIconSignal(false);
        this->showNormal();
        foglalt = false;
    }
}

void MainWindow::showContextMenu(const QPoint &pt) {
    QMenu *menu = ((SaTextEdit*) mFr)->createStandardContextMenu();
    menu->addSeparator();
    if (mFr->objectName() != "sugoTextBrowser") {
        QAction *keresAction;
        if (mFr->objectName() == "valaszTextEdit" &&
                ((QTextEdit*) mFr)->textCursor().selectedText() != "") {
            QAction *keresAction = new QAction(satr("Cedict"), this);
            keresAction->connect(keresAction, SIGNAL(triggered()), this, SLOT(cedictKeres()));
            menu->addAction(keresAction);
        }
        QAction *fontAction = new QAction(satr("Betűtípus"), this);
        fontAction->connect(fontAction, SIGNAL(triggered()), this, SLOT(showFontDialog()));
        menu->addAction(fontAction);
        QAction *betuSzinAction = new QAction(satr("Betűszín"), this);
        betuSzinAction->connect(betuSzinAction, SIGNAL(triggered()), this, SLOT(showBetuszinDialog()));
        menu->addAction(betuSzinAction);
        if (mFr->objectName() == "valaszTextEdit") {
            QAction *fontActionCh = new QAction(satr("Betűtípus")+"Ch", this);
            fontActionCh->connect(fontActionCh, SIGNAL(triggered()), this, SLOT(showFontDialogCh()));
            menu->addAction(fontActionCh);
            QAction *betuSzinActionCh = new QAction(satr("Betűszín")+"Ch", this);
            betuSzinActionCh->connect(betuSzinActionCh, SIGNAL(triggered()), this, SLOT(showBetuszinDialogCh()));
            menu->addAction(betuSzinActionCh);
            QAction *fontActionPin = new QAction(satr("Betűtípus")+"Pin", this);
            fontActionPin->connect(fontActionPin, SIGNAL(triggered()), this, SLOT(showFontDialogPin()));
            menu->addAction(fontActionPin);
            QAction *betuSzinActionPin = new QAction(satr("BetűszínPin"), this);
            betuSzinActionPin->connect(betuSzinActionPin, SIGNAL(triggered()),
                    this, SLOT(showBetuszinDialogPin()));
            menu->addAction(betuSzinActionPin);
        }
        QAction *hatterSzinAction = new QAction(satr("Háttérszín"), this);
        hatterSzinAction->connect(hatterSzinAction, SIGNAL(triggered()),
                this, SLOT(showHatterszinDialog()));
        menu->addAction(hatterSzinAction);
    }
    menu->addMenu(nyelvMenu);
    menu->exec(pt);
    delete menu;
}

void MainWindow::showBetuszinDialog() {
    colorDialog->disconnect(colorDialog, SIGNAL(colorSelected(QColor)),
            this, SLOT(setHatterSzin(QColor)));
    colorDialog->disconnect(colorDialog, SIGNAL(colorSelected(QColor)),
            this, SLOT(setBetuSzin(QColor)));
    colorDialog->disconnect(colorDialog, SIGNAL(colorSelected(QColor)),
            this, SLOT(setBetuSzinCh(QColor)));
    colorDialog->disconnect(colorDialog, SIGNAL(colorSelected(QColor)),
            this, SLOT(setBetuSzinPin(QColor)));

    colorDialog->setCurrentColor(((SaTextEdit*)mFr)->betuSzin);

    colorDialog->connect(colorDialog, SIGNAL(colorSelected(QColor)),
            this, SLOT(setBetuSzin(QColor)));
    colorDialog->show();
}

void MainWindow::showBetuszinDialogCh() {
    colorDialog->disconnect(colorDialog, SIGNAL(colorSelected(QColor)),
            this, SLOT(setHatterSzin(QColor)));
    colorDialog->disconnect(colorDialog, SIGNAL(colorSelected(QColor)),
            this, SLOT(setBetuSzin(QColor)));
    colorDialog->disconnect(colorDialog, SIGNAL(colorSelected(QColor)),
            this, SLOT(setBetuSzinCh(QColor)));
    colorDialog->disconnect(colorDialog, SIGNAL(colorSelected(QColor)),
            this, SLOT(setBetuSzinPin(QColor)));

    QColor col = QColor(
            (valaszTextEditStilusCh.split("|")[1]).toInt(),
            (valaszTextEditStilusCh.split("|")[2]).toInt(),
            (valaszTextEditStilusCh.split("|")[3]).toInt() );
    colorDialog->setCurrentColor(col);

    colorDialog->connect(colorDialog, SIGNAL(colorSelected(QColor)),
            this, SLOT(setBetuSzinCh(QColor)));
    colorDialog->show();
}

void MainWindow::showBetuszinDialogPin() {
    colorDialog->disconnect(colorDialog, SIGNAL(colorSelected(QColor)),
            this, SLOT(setHatterSzin(QColor)));
    colorDialog->disconnect(colorDialog, SIGNAL(colorSelected(QColor)),
            this, SLOT(setBetuSzin(QColor)));
    colorDialog->disconnect(colorDialog, SIGNAL(colorSelected(QColor)),
            this, SLOT(setBetuSzinCh(QColor)));
    colorDialog->disconnect(colorDialog, SIGNAL(colorSelected(QColor)),
            this, SLOT(setBetuSzinPin(QColor)));

    QColor col = QColor(
            (valaszTextEditStilusPin.split("|")[1]).toInt(),
            (valaszTextEditStilusPin.split("|")[2]).toInt(),
            (valaszTextEditStilusPin.split("|")[3]).toInt() );
    colorDialog->setCurrentColor(col);

    colorDialog->connect(colorDialog, SIGNAL(colorSelected(QColor)),
            this, SLOT(setBetuSzinPin(QColor)));
    colorDialog->show();
}

void MainWindow::showHatterszinDialog() {
    colorDialog->disconnect(colorDialog, SIGNAL(colorSelected(QColor)),
            this, SLOT(setHatterSzin(QColor)));
    colorDialog->disconnect(colorDialog, SIGNAL(colorSelected(QColor)),
            this, SLOT(setBetuSzin(QColor)));
    colorDialog->disconnect(colorDialog, SIGNAL(colorSelected(QColor)),
            this, SLOT(setBetuSzinCh(QColor)));
    colorDialog->disconnect(colorDialog, SIGNAL(colorSelected(QColor)),
            this, SLOT(setBetuSzinPin(QColor)));

    colorDialog->setCurrentColor(((SaTextEdit*)mFr)->hatterSzin);

    colorDialog->connect(colorDialog, SIGNAL(colorSelected(QColor)),
            this, SLOT(setHatterSzin(QColor)));
    colorDialog->show();
}

void MainWindow::showFontDialog() {
    fontDialog->disconnect(fontDialog, SIGNAL(fontSelected(QFont)),
            this, SLOT(setSaFont(QFont)));
    fontDialog->disconnect(fontDialog, SIGNAL(fontSelected(QFont)),
            this, SLOT(setSaFontCh(QFont)));
    fontDialog->disconnect(fontDialog, SIGNAL(fontSelected(QFont)),
            this, SLOT(setSaFontPin(QFont)));

    fontDialog->setCurrentFont(((SaTextEdit*) mFr)->font());
    fontDialog->connect(fontDialog, SIGNAL(fontSelected(QFont)),
            this, SLOT(setSaFont(QFont)));
    fontDialog->show();
}

void MainWindow::showFontDialogCh() {
    fontDialog->disconnect(fontDialog, SIGNAL(fontSelected(QFont)),
            this, SLOT(setSaFont(QFont)));
    fontDialog->disconnect(fontDialog, SIGNAL(fontSelected(QFont)),
            this, SLOT(setSaFontCh(QFont)));
    fontDialog->disconnect(fontDialog, SIGNAL(fontSelected(QFont)),
            this, SLOT(setSaFontPin(QFont)));

    QFont f = ((SaTextEdit*) mFr)->font();
    f.setFamily(valaszTextEditStilusCh.split("|")[0]);
    f.setPointSize((valaszTextEditStilusCh.split("|")[4]).toInt());
    f.setBold((valaszTextEditStilusCh.split("|")[5]).toInt());
    fontDialog->setCurrentFont(f);
    fontDialog->connect(fontDialog, SIGNAL(fontSelected(QFont)),
            this, SLOT(setSaFontCh(QFont)));
    fontDialog->show();
}

void MainWindow::showFontDialogPin() {
    fontDialog->disconnect(fontDialog, SIGNAL(fontSelected(QFont)),
            this, SLOT(setSaFont(QFont)));
    fontDialog->disconnect(fontDialog, SIGNAL(fontSelected(QFont)),
            this, SLOT(setSaFontCh(QFont)));
    fontDialog->disconnect(fontDialog, SIGNAL(fontSelected(QFont)),
            this, SLOT(setSaFontPin(QFont)));

    QFont f = ((SaTextEdit*) mFr)->font();
    f.setFamily(valaszTextEditStilusPin.split("|")[0]);
    f.setPointSize((valaszTextEditStilusPin.split("|")[4]).toInt());
    f.setBold((valaszTextEditStilusPin.split("|")[5]).toInt());
    fontDialog->setCurrentFont(f);
    fontDialog->connect(fontDialog, SIGNAL(fontSelected(QFont)),
            this, SLOT(setSaFontPin(QFont)));
    fontDialog->show();
}

void MainWindow::setBetuSzin(QColor col) {
//    qWarning("setBetuSzin:");
    QString st = ((SaTextEdit*) mFr)->toPlainText();
    ((SaTextEdit*) mFr)->setPlainText("");
    ((SaTextEdit*) mFr)->setBetuSzin(col);
    ((SaTextEdit*) mFr)->setPlainText(st);
    if (mFr->objectName() == QString::fromUtf8("valaszTextEdit")) {
        valaszTextEditStilus =
                valaszTextEditStilus.split("|")[0] + "|" +
                QString::number(col.red()) + "|" +
                QString::number(col.green()) + "|" +
                QString::number(col.blue()) + "|" +
                valaszTextEditStilus.split("|")[4] + "|" +
                valaszTextEditStilus.split("|")[5] + "|" +
                valaszTextEditStilus.split("|")[6] + "|" +
                valaszTextEditStilus.split("|")[7] + "|" +
                valaszTextEditStilus.split("|")[8];
    } else if (mFr->objectName() == QString::fromUtf8("pipeTextEdit")) {
        pipeTextEditStilus =
                pipeTextEditStilus.split("|")[0] + "|" +
                QString::number(col.red()) + "|" +
                QString::number(col.green()) + "|" +
                QString::number(col.blue()) + "|" +
                pipeTextEditStilus.split("|")[4] + "|" +
                pipeTextEditStilus.split("|")[5] + "|" +
                pipeTextEditStilus.split("|")[6] + "|" +
                pipeTextEditStilus.split("|")[7] + "|" +
                pipeTextEditStilus.split("|")[8];
    }
    st = kerdezTextEdit->textCursor().selectedText();
    if (st == "") {
        st = kerdezTextEdit->toPlainText();
    }
    kerdezTextEdit->setPlainText(kerdezTextEdit->toPlainText());
    helyiValasztKiir(st);
}

void MainWindow::setBetuSzinCh(QColor col) {
    //qWarning("setBetuSzinCh:");
    ((SaTextEdit*) mFr)->hide();
    valaszTextEditStilusCh =
            valaszTextEditStilusCh.split("|")[0] + "|" +
            QString::number(col.red()) + "|" +
            QString::number(col.green()) + "|" +
            QString::number(col.blue()) + "|" +
            valaszTextEditStilusCh.split("|")[4] + "|" +
            valaszTextEditStilusCh.split("|")[5];
    QString st = kerdezTextEdit->textCursor().selectedText();
    if (st == "") {
        st = kerdezTextEdit->toPlainText();
    }
    helyiValasztKiir(st);
    ((SaTextEdit*) mFr)->show();
}

void MainWindow::setBetuSzinPin(QColor col) {
    //qWarning("setBetuSzinPin:");
    ((SaTextEdit*) mFr)->hide();
    valaszTextEditStilusPin =
            valaszTextEditStilusPin.split("|")[0] + "|" +
            QString::number(col.red()) + "|" +
            QString::number(col.green()) + "|" +
            QString::number(col.blue()) + "|" +
            valaszTextEditStilusPin.split("|")[4] + "|" +
            valaszTextEditStilusPin.split("|")[5];
    QString st = kerdezTextEdit->textCursor().selectedText();
    if (st == "") {
        st = kerdezTextEdit->toPlainText();
    }
    helyiValasztKiir(st);
    ((SaTextEdit*) mFr)->show();
}

void MainWindow::setHatterSzin(QColor col) {
    QString st = ((SaTextEdit*) mFr)->toPlainText();
    ((SaTextEdit*) mFr)->setPlainText("");
    ((SaTextEdit*) mFr)->setHattertSzin(col);
    ((SaTextEdit*) mFr)->setPlainText(st);
    if (mFr->objectName() == QString::fromUtf8("valaszTextEdit")) {
        valaszTextEditStilus =
                valaszTextEditStilus.split("|")[0] + "|" +
                valaszTextEditStilus.split("|")[1] + "|" +
                valaszTextEditStilus.split("|")[2] + "|" +
                valaszTextEditStilus.split("|")[3] + "|" +
                QString::number(col.red()) + "|" +
                QString::number(col.green()) + "|" +
                QString::number(col.blue()) + "|" +
                valaszTextEditStilus.split("|")[7] + "|" +
                valaszTextEditStilus.split("|")[8];
    } else if (mFr->objectName() == QString::fromUtf8("pipeTextEdit")) {
        pipeTextEditStilus =
                pipeTextEditStilus.split("|")[0] + "|" +
                pipeTextEditStilus.split("|")[1] + "|" +
                pipeTextEditStilus.split("|")[2] + "|" +
                pipeTextEditStilus.split("|")[3] + "|" +
                QString::number(col.red()) + "|" +
                QString::number(col.green()) + "|" +
                QString::number(col.blue()) + "|" +
                pipeTextEditStilus.split("|")[7] + "|" +
                pipeTextEditStilus.split("|")[8];
    }
    st = kerdezTextEdit->textCursor().selectedText();
    if (st == "") {
        st = kerdezTextEdit->toPlainText();
    }
    helyiValasztKiir(st);
}

void MainWindow::setSaFont(QFont font) {
    //qWarning("setSaFont:");
    ((SaTextEdit*) mFr)->hide();
    ((SaTextEdit*) mFr)->setFont(font);
    valaszTextEditStilus =
            font.family() + "|" +
            valaszTextEditStilus.split("|")[1] + "|" +
            valaszTextEditStilus.split("|")[2] + "|" +
            valaszTextEditStilus.split("|")[3] + "|" +
            valaszTextEditStilus.split("|")[4] + "|" +
            valaszTextEditStilus.split("|")[5] + "|" +
            valaszTextEditStilus.split("|")[6] + "|" +
            QString::number(font.pointSize()) + "|" +
            (font.bold() ? "1" : "0");
    //qWarning("MainWindow::setSaFont: valaszTextEditStilus: " + valaszTextEditStilus.toLatin1());
    QString st = kerdezTextEdit->textCursor().selectedText();
    if (st == "") {
        st = kerdezTextEdit->toPlainText();
    }
    helyiValasztKiir(st);
    ((SaTextEdit*) mFr)->show();
}

void MainWindow::setSaFontCh(QFont font) {
    ((SaTextEdit*) mFr)->hide();
    valaszTextEditStilusCh =
            font.family() + "|" +
            valaszTextEditStilusCh.split("|")[1] + "|" +
            valaszTextEditStilusCh.split("|")[2] + "|" +
            valaszTextEditStilusCh.split("|")[3] + "|" +
            QString::number(font.pointSize()) + "|" +
            (font.bold() ? "1" : "0");
    //qWarning("MainWindow::setSaFontCh: valaszTextEditStilusCh: " + valaszTextEditStilusCh.toLatin1());
    QString st = kerdezTextEdit->textCursor().selectedText();
    if (st == "") {
        st = kerdezTextEdit->toPlainText();
    }
    helyiValasztKiir(st);
    ((SaTextEdit*) mFr)->show();
}

void MainWindow::setSaFontPin(QFont font) {
    ((SaTextEdit*) mFr)->hide();
    valaszTextEditStilusPin =
            font.family() + "|" +
            valaszTextEditStilusPin.split("|")[1] + "|" +
            valaszTextEditStilusPin.split("|")[2] + "|" +
            valaszTextEditStilusPin.split("|")[3] + "|" +
            QString::number(font.pointSize()) + "|" +
           (font.bold() ? "1" : "0");
    //qWarning("MainWindow::setSaFontPin: valaszTextEditStilusPin: " + valaszTextEditStilusPin.toLatin1());
    QString st = kerdezTextEdit->textCursor().selectedText();
    if (st == "") {
        st = kerdezTextEdit->toPlainText();
    }
    helyiValasztKiir(st);
    ((SaTextEdit*) mFr)->show();
}

void MainWindow::nyelvValtas(QString lng) {
    if (lng != nyelv) {
        nyelv = lng;
        nyelvBeallitas();
    }
 }

void MainWindow::nyelvBeallitas() {
    nyelvSt = fajlToQString(exePath + "../lang/lang_" + nyelv + ".lng");
    nyelvMenu->setIcon(QIcon(exePath + "../lang/flag_" + nyelv + ".png"));
    nyelvMenu->setTitle(satr("Nyelv"));
    nyelvMenu2->setIcon(QIcon(exePath + "../lang/flag_" + nyelv + ".png"));
    nyelvMenu2->setTitle(satr("Nyelv"));
    ui->tabWidget->setTabText(0, satr("Kereső"));
    ui->tabWidget->setTabText(1, satr("Beállítások"));
    ui->tabWidget->setTabText(2, satr("Súgó"));
    ui->tabWidget->setTabText(3, satr("Konzol"));
    ui->checkBox->setText(satr("Pipe output frissít"));
    ui->checkBox_2->setText(satr("Saját szótár használat"));
    ui->checkBox_4->setText(satr("Nagybetűs pinek"));
    ui->checkBox_5->setText(satr("Külön szó"));
    ui->checkBox_3->setText(satr("Minimalizálás a tálcára"));
    ui->bLabel1->setText(satr("CEDICT és JMdict szótár fájl (pl. cedict_ts.u8, JMdict_e.xml) helye:"));
    ui->bLabel2->setText(satr("Saját szótár fájl helye:"));
    ui->bLabel3->setText(satr("Konvertálandó forrás *.srt fájl:"));
    ui->bLabel4->setText(satr("Cél *.srt fájl:"));
    celSrtFajlNevEll(); //bLabel5 beírása
    ui->label->setText(satr("Pipe név") + ": " + getPipeNev());
    ui->bButton1->setText(satr("Tallóz"));
    ui->bButton2->setText(satr("Tallóz"));
    ui->bButton3->setText(satr("Tallóz"));
    ui->bButton4->setText(satr("Tallóz"));
    ui->bMentButton->setText(satr("Ment"));
    ui->bKonvertalButton->setText(satr("Konvertál"));
    ui->kijelzesTorlesButton->setText(satr("Kijelző törlése"));
    ui->visszaButton->setToolTip("Alt + " + satr("Bal"));
    ui->eloreButton->setToolTip("Alt + " + satr("Jobb"));
    tab2NyelvMenu->hide();
    nyelvMenu2->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Minimum);
    tab2NyelvMenu->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Minimum);
    tab2NyelvMenu->show();
    QString st = satr("Súgó: lásd az xx fájlt.");
    if (nyelv == "eng") {
        st = st.replace("xx", "<tt>readme.md</tt>");
    } else {
        st = st.replace("xx", "<tt>md\\readme_" + nyelv + ".md</tt>");
    }
    ui->sugoTextBrowser->setHtml(st);
}

QString MainWindow::satr(QByteArray ba) {
    QString st = QString::fromUtf8(ba);
    QString st2;
    QString k1 = QString::fromUtf8("<eredeti>") + st +
            QString::fromUtf8("</eredeti><fordítás>");
    int p1 = nyelvSt.indexOf(k1);
    if (p1 > -1) {
        p1 += k1.length();
        int p2 = nyelvSt.indexOf(QString::fromUtf8("</fordítás>"), p1);
        if (p2 > -1) {
            st2 = nyelvSt.mid(p1, p2-p1);
        } else {
            st2 = st;
        }
    } else {
        st2 = st;
    }
    return st2;
}

void MainWindow::mousePressEvent(QMouseEvent *e) {
    QPoint p = e->globalPos();
    mFr = egerAlatt(p);
    if (mFr) {
//        qWarning("mFr: " + mFr->objectName().toLatin1());
            if (e->button() == Qt::RightButton) {
                if (mFr->objectName() == QString::fromUtf8("kerdezTextEdit") ||
                        mFr->objectName() == QString::fromUtf8("valaszTextEdit") ||
                        mFr->objectName() == QString::fromUtf8("sugoTextBrowser") ||
                        mFr->objectName() == QString::fromUtf8("pipeTextEdit")) {
                    this->showContextMenu(e->globalPos());
                } else {
                    QMainWindow::mousePressEvent(e);
                }
            }
    } else {
        QMainWindow::mousePressEvent(e);
    }
}

QWidget *MainWindow::egerAlatt(QPoint p) {
    QWidget *te = a->widgetAt(p);
    if (te->objectName() == "qt_scrollarea_viewport") {
        te = (QWidget*) te->parent();
    }
    return te;
}

void MainWindow::szoListVisszaUgras() {
    disconnect(kerdezTextEdit, SIGNAL(textChanged()),
            this, SLOT(helyiValasz()));
    if (sz->szoListIdx > -1) {
        sz->szoListIdx--;
        ui->szoListLabel->setText(". " + QString::number(sz->szoListIdx + 1) + " / " +
                QString::number(sz->szoList.size()) + " .");
    }
    if (sz->szoListIdx > -1) {
        kerdezTextEdit->setPlainText(sz->szoList[sz->szoListIdx]);
        helyiValasztKiir(kerdezTextEdit->toPlainText());
    } else {
        kerdezTextEdit->setPlainText("");
        valaszTextEdit->setPlainText("");
    }
    connect(kerdezTextEdit, SIGNAL(textChanged()),
            this, SLOT(helyiValasz()));
}

void MainWindow::szoListEloreUgras() {
    disconnect(kerdezTextEdit, SIGNAL(textChanged()),
            this, SLOT(helyiValasz()));
    if (sz->szoListIdx < sz->szoList.size() - 1) {
        sz->szoListIdx++;
        ui->szoListLabel->setText(". " + QString::number(sz->szoListIdx + 1) + " / " +
                QString::number(sz->szoList.size()) + " .");
    }
    kerdezTextEdit->setPlainText(sz->szoList[sz->szoListIdx]);
    helyiValasztKiir(kerdezTextEdit->toPlainText());
    connect(kerdezTextEdit, SIGNAL(textChanged()),
            this, SLOT(helyiValasz()));
}

bool MainWindow::event(QEvent *e) {
    if (e->type() == QEvent::WindowActivate) {
        if (ui->tabWidget->currentIndex() == 0) {
            kerdezTextEdit->selTorol();
            kerdezTextEdit->setFocus();
            kerdezTextEdit->selectAll();
        }
    } else if (e->type() != QEvent::None) {
        return QMainWindow::event(e);
    }
}

void MainWindow::helyiValasztKiir(QString kerdezSt) {
    QString st = ui->comboBox->currentText() + "|" +
            kerdezSt;
    QString valaszSt = sz->ValaszFv(st);
    if (st.startsWith("FvChToEn|") || st.startsWith("FvJpToEn|")) {
        QStringList valaszSorL = valaszSt.split("\n");
        QString v2St = "";
        for (int i = 0; i < valaszSorL.size(); i++) {
            QStringList v2StL = valaszSorL[i].split(SZEPA6);
            if (v2StL.size() % 2 != 1) {
                v2St = v2St + valaszSt;
            } else {
                QStringList vStilusL = valaszTextEditStilus.split("|");
                QStringList vStilusChL = valaszTextEditStilusCh.split("|");
                QStringList vStilusPinL = valaszTextEditStilusPin.split("|");
                v2St = v2St +
                        "<span style=\""
                        "font-family: " + vStilusChL[0] + ";" +
                        "color:rgb(" + vStilusChL[1] + "," + vStilusChL[2] + "," + vStilusChL[3] + ");" +
                        "font-size: " + vStilusChL[4] + "px;" +
                        "font-weight: " + (vStilusChL[5] == "1" ? "bold" : "normal") + ";" +
                        + "\">" + v2StL[0] + "</span>";
                for (int j=1; j<v2StL.size(); j+=2) {
                    QString ujSor = "";
                    if (j < v2StL.size() - 3 || i < valaszSorL.size() - 1) {
                        ujSor = "<br>";
                    }
                    v2St = v2St +
                            "<span style=\""
                            "font-family: " + vStilusL[0] + ";" +
                            "color:rgb(" + vStilusL[1] + "," + vStilusL[2] + "," + vStilusL[3] + ");" +
                            "background-color:rgb(" + vStilusL[4] + "," + vStilusL[5] + "," + vStilusL[6] + ");" +
                            "font-size: " + vStilusL[7] + "px;" +
                            "font-weight: " + (vStilusL[8] == "1" ? "bold" : "normal") + ";" +
                            + "\">" + " - [" + "</span>"

                            "<span style=\""
                            "font-family: " + vStilusPinL[0] + ";" +
                            "color:rgb(" + vStilusPinL[1] + "," + vStilusPinL[2] + "," + vStilusPinL[3] + ");" +
                            "font-size: " + vStilusPinL[4] + "px;" +
                            "font-weight: " + (vStilusPinL[5] == "1" ? "bold" : "normal") + ";" +
                            + "\">" + v2StL[j] + "</span>"

                            "<span style=\""
                            "font-family: " + vStilusL[0] + ";" +
                            "color:rgb(" + vStilusL[1] + "," + vStilusL[2] + "," + vStilusL[3] + ");" +
                            "background-color:rgb(" + vStilusL[4] + "," + vStilusL[5] + "," + vStilusL[6] + ");" +
                            "font-size: " + vStilusL[7] + "px;" +
                            "font-weight: " + (vStilusL[8] == "1" ? "bold" : "normal") + ";" +
                            + "\">" + "]: " + v2StL[j+1] + ujSor + "</span>";
                }
            }
        }
        //qWarning("h1: " + v2St.toLatin1());
        valaszTextEdit->setText(v2St);
    } else if (st.startsWith("FvPinToPin15|")) {
        valaszTextEdit->setPlainText(valaszSt.replace(SZEPA3, "").replace(PINSZOKOZ, " "));
    } else {
        valaszTextEdit->setPlainText(valaszSt);
    }

}

void MainWindow::cedictKeres() {
    QString st = valaszTextEdit->textCursor().selectedText();
    kerdezTextEdit->setText(st);
}
