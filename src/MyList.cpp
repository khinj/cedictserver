﻿#pragma once
#include "MyList.h"

template <typename T>
long MyList<T>::firstMatch(QString s1, QString s2) {
    long i = 0;
    while (i < s1.size() && i < s2.size() && s1.mid(i, 1) == s2.mid(i, 1)) {
        i++;
    }
    return i;
}

template <typename T>
MyList<T>::MyList() {

}

template <typename T>
MyList<T>::~MyList<T>() {

}

template <typename T>
long MyList<T>::beszur(QString st0, T value) {
	QString st = st0.toLower();
    long idx = this->keresBeszur(st);
    stL.insert(idx, st);
    vL.insert(idx, value);
    return idx;
}

template <typename T>
void MyList<T>::beszur(long idx, QString st0, T value) {
    QString st = st0.toLower();
    stL.insert(idx, st);
    vL.insert(idx, value);
}

template <typename T>
long MyList<T>::keresBeszur(QString keresSt0) {
	//Új elem beszúrásához használatos.
    //Megkeresi azt legkisebb stL[idx]-et, amelyre LCase(keresSt) <= stL[idx],
    //  ha nincs ilyen, akkor stL.size() -t ad vissza.
	
	QString keresSt = keresSt0.toLower();
    if (stL.size() == 0 || keresSt <= stL[0]) {
        return 0;
    } else if (stL[stL.size()-1] < keresSt) {
        return stL.size();
	} else {
        long p1 = 0;
        long p2 = stL.size() - 1;
        do {
            long p = (p1 + p2 + 1) / 2;
            if (keresSt <= stL[p]) {
                p2 = p;
			} else {
                p1 = p;
			}
            //itt mindig teljesül, hogy stL[p1] < keresSt <= stL[p2]
        } while (p1 + 1 < p2);
        return p2;
	}
}

template <typename T>
long MyList<T>::keresSzotar(QString keresSt0) {
    //Keresi azt az idx-et, amelyre stL[idx] eleje a leghosszabban egyezik
	//	LCase(keresSt0)-lal.
    //qWarning("keresSt0 = " + keresSt0.toUtf8());
    if (keresSt0 == "" || this->size() == 0) {
		return -1;
	} else {
		QString keresSt = keresSt0.toLower();
        QString st1, st2;
        long idx = this->keresBeszur(keresSt);
        if (idx > stL.size() - 1) {
            idx = stL.size() - 1;
        }
        st2 = stL[idx];
        while (0 < idx) {
            st1 = stL[idx - 1];
            if (st1.left(1) != keresSt.left(1) ||
                    keresSt.startsWith(st2)) {
                break;
            }
            st2 = st1;
            idx--;
        }
        if ( keresSt.startsWith(st2)) {
			return idx;
		} else {
			return -1;
		}
	}
}

template <typename T>
QString MyList<T>::getKey(long idx) {
	return stL[idx];
}

template <typename T>
T MyList<T>::getValue(long idx) {
	return vL[idx];
}

template <typename T>
void MyList<T>::setValue(long idx, T value) {
	vL[idx] = value;
}

template <typename T>
void MyList<T>::torol() {
    stL.clear();
    vL.clear();
}

template <typename T>
int MyList<T>::size() {
    return stL.size();
}
