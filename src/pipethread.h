﻿#ifndef PIPETHREAD_H
#define PIPETHREAD_H

#include <QThread>


class PipeThread : public QThread
{
    Q_OBJECT
public:
    explicit PipeThread(QObject *parent = 0);
    void StartPipe();
    void run();
    
signals:
    
public slots:
    
};

#endif // PIPETHREAD_H
