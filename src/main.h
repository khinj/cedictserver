﻿#ifndef MAIN_H
#define MAIN_H

#include <QApplication>
#include <QtGui>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "pipethread.h"
#include "Szotar.h"
#include "tcpipserver.h"

QApplication *a;
PipeThread *p;
Szotar *sz;
MainWindow *w;
QString iniFajlSt;
QString iniFajlNev;
bool pipeThreadVege;
int maxKarakterSzam;
QString exePath;

LPTSTR QStringToLPTSTR(QString lineSt);
QString LPTSTRToQString(LPTSTR lineSt);
void setParameter(QString *forras, QString parNev, QString ertek);
QString getParameter(QString *forras, QString parNev, QString defaultErtek = NULL);
QString fajlToQString(QString fajlNev);
void QStToFajl(QString fajlNev, QString st, bool kellBOM);
void iniBeolvas();
void iniKiir();
QString qRectToqString(QRect r);
QRect qStringToqRect(QString lineSt);
void kilepes();
QString getStilus(SaTextEdit *te);
void setStilus(SaTextEdit *te, QString st);

#endif // MAIN_H
