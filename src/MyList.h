﻿#pragma once
#include <QStringList>

template <typename T>
class MyList
{
public:
    MyList();
    ~MyList();
    long beszur(QString st0, T value);
    void beszur(long idx, QString st0, T value);
    long keresBeszur(QString keresSt0);
	long keresSzotar(QString keresSt0);
	QString getKey(long idx);
    T getValue(long idx);
    void setValue(long idx, T value);
    int size();
    void torol();
    static long firstMatch(QString s1, QString s2);

	QStringList stL;
    QList<T> vL;
};
