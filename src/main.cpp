﻿// http://doc.qt.io/archives/qt-4.8/qt-network-network-chat-example.html

#include "main.h"

int main(int argc, char *argv[])
{
    a = new QApplication(argc, argv);
    QFileInfo fajlInfo = QFileInfo(a->arguments()[0]);
    exePath = fajlInfo.path()+"/";

    QPixmap pixmap(exePath + "Splash2.png");
    QSplashScreen splash(pixmap);
    splash.show();
    a->processEvents();

    w = new MainWindow();
    iniBeolvas();

    p = new PipeThread;
    p->start();
    sz = new Szotar;

    QStringList stL = getParameter(&iniFajlSt, "szoList").split("|");
    for (int i=0; i<stL.size(); i++) {
        if (stL[i] != "") {
            sz->szoList.append(stL[i].replace(QString::fromUtf8("÷1"), "\n"));
        }
    }
    sz->szoListIdx = sz->szoList.size()-1;
    w->ui->visszaButton->setIcon(QIcon(exePath + "img_back.png"));
    w->ui->eloreButton->setIcon(QIcon(exePath + "img_forward.png"));
    QObject::connect(w->ui->visszaButton, SIGNAL(clicked()), w, SLOT(szoListVisszaUgras()));
    QObject::connect(w->ui->eloreButton, SIGNAL(clicked()), w, SLOT(szoListEloreUgras()));
    QString st = ". " + QString::number(sz->szoListIdx + 1) + " / " +
            QString::number(sz->szoList.size()) + " .";
    w->ui->szoListLabel->setText(st);
    w->show();
    splash.finish(w);
    w->ui->bMentButton->setStyleSheet(w->ui->bButton1->styleSheet());
    pipeThreadVege = false;

    TcpIPServer server;
//    qsrand(QTime(0,0,0).secsTo(QTime::currentTime()));
//    server.exec();

//    Main2* m2a = new Main2;
//    m2a->main2Fn(a);
//    Main2* m2b = new Main2;
//    m2b->main2Fn(a);

    sz->eredetiCedictSzotarFajlNev = "";
    sz->eredetiJMdictSzotarFajlNev = "";
    sz->eredetiLocalSzotarFajlNev = "";
    sz->szotarBeolvas();
    sz->localSzotarBeolvas();

    return a->exec();
}

void iniBeolvas() {
    if (!QFile(exePath + "CedictServer.ini").exists()) {
        QFile::copy(exePath + "CedictServer.ini.template", exePath + "CedictServer.ini");
    }
    iniFajlNev = exePath + "CedictServer.ini";
    iniFajlSt = fajlToQString(iniFajlNev);
    //qWarning("iniSt = " + iniSt.toUtf8());

    w->setGeometry(qStringToqRect(getParameter(&iniFajlSt, "foablak")));
    w->ui->checkBox->setChecked(getParameter(&iniFajlSt, "pipePipa") == "1");
    w->ui->tabWidget->setCurrentIndex(getParameter(&iniFajlSt, "aktTab").toInt());

    w->ui->comboBox->setCurrentIndex(getParameter(&iniFajlSt, "comboBox").toInt());
    maxKarakterSzam = getParameter(&iniFajlSt, "MAXKARAKTERSZAM").toInt();
    w->ui->checkBox_2->setChecked(getParameter(&iniFajlSt, "lokSzoPipa") == "1");
    w->ui->checkBox_3->setChecked(getParameter(&iniFajlSt, "minTray") == "1");
    w->ui->checkBox_4->setChecked(getParameter(&iniFajlSt, "PinPipa") == "1");
    w->ui->checkBox_5->setChecked(getParameter(&iniFajlSt, "SelSepa", "1") == "1");

    QString st;
    st = getParameter(&iniFajlSt, "kerdezTextEditStilus");
    setStilus((SaTextEdit*) w->kerdezTextEdit, st);
    w->valaszTextEditStilus = getParameter(&iniFajlSt, "valaszTextEditStilus");
    setStilus((SaTextEdit*) w->valaszTextEdit, w->valaszTextEditStilus);
    w->valaszTextEditStilusCh = getParameter(&iniFajlSt, "valaszTextEditStilusCh");
    w->valaszTextEditStilusPin = getParameter(&iniFajlSt, "valaszTextEditStilusPin");
    w->pipeTextEditStilus = getParameter(&iniFajlSt, "pipeTextEditStilus");
    setStilus((SaTextEdit*) w->pipeTextEdit, w->pipeTextEditStilus);

    QFont font = w->ui->sugoTextBrowser->font();
    font.setPointSize(getParameter(&iniFajlSt, "sugoBetuMeret").toInt());
    w->ui->sugoTextBrowser->setFont(font);

    w->ui->bTextEdit1->setPlainText(getParameter(&iniFajlSt, QString::fromUtf8("CEDICT_SZÓTÁRFÁJL")));
    w->ui->bTextEdit1_2->setPlainText(getParameter(&iniFajlSt, QString::fromUtf8("JMDICT_SZÓTÁRFÁJL")));
    w->ui->bTextEdit2->setPlainText(getParameter(&iniFajlSt, QString::fromUtf8("SAJÁT_SZÓTÁRFÁJL")));

    //Ez a sorrend jó (máskülönben bTextEdit3 SIGNAL és bTextEdit4 üressége miatt figyelmeztetés):
    w->ui->bTextEdit4->setPlainText(getParameter(&iniFajlSt, QString::fromUtf8("célSrt")));
    w->ui->bTextEdit3->setPlainText(getParameter(&iniFajlSt, QString::fromUtf8("forrásSrt")));

    QString spAranySt = getParameter(&iniFajlSt, "SplitterArany");
    QList<int> spArany;
    spArany << spAranySt.split("/")[0].toInt() << spAranySt.split("/")[1].toInt();
    w->ui->splitter->setSizes(spArany);

    w->nyelv = getParameter(&iniFajlSt, "Nyelv");
    w->nyelvMenu = new QMenu();
    QStringList langL = QDir(exePath + "../lang/", "lang_*.lng").entryList();
    QSignalMapper *sm = new QSignalMapper;
    for (int i=0; i<langL.size(); i++) {
        QString st = langL[i];
        st = st.replace("lang_", "");
        st = st.replace(".lng", "");
        QAction *nyelvAction = w->nyelvMenu->addAction(
                QIcon(exePath + "../lang/flag_" + st + ".png"), st);
        nyelvAction->connect(nyelvAction, SIGNAL(triggered()), sm, SLOT(map()));
        sm->setMapping(nyelvAction, st);
        QObject::connect(sm, SIGNAL(mapped(QString)),
                w, SIGNAL(nyelvSignal(QString)));
        nyelvAction = w->nyelvMenu2->addAction(
                QIcon(exePath + "../lang/flag_" + st + ".png"), st);
        nyelvAction->connect(nyelvAction, SIGNAL(triggered()), sm, SLOT(map()));
        sm->setMapping(nyelvAction, st);
        QObject::connect(sm, SIGNAL(mapped(QString)),
                w, SIGNAL(nyelvSignal(QString)));
    }
    w->nyelvBeallitas();
}

void iniKiir(){
    setParameter(&iniFajlSt, "foablak", qRectToqString(w->geometry()));
    setParameter(&iniFajlSt, "aktTab", (QString::number(w->ui->tabWidget->currentIndex())));
    setParameter(&iniFajlSt, "comboBox", (QString::number(w->ui->comboBox->currentIndex())));
    setParameter(&iniFajlSt, "pipePipa", (w->ui->checkBox->isChecked() ? "1" : "0"));
    setParameter(&iniFajlSt, "lokSzoPipa", (w->ui->checkBox_2->isChecked() ? "1" : "0"));
    setParameter(&iniFajlSt, "PinPipa", (w->ui->checkBox_4->isChecked() ? "1" : "0"));
    setParameter(&iniFajlSt, "SelSepa", (w->ui->checkBox_5->isChecked() ? "1" : "0"));
    setParameter(&iniFajlSt, "minTray", (w->ui->checkBox_3->isChecked() ? "1" : "0"));

    setParameter(&iniFajlSt, "kerdezTextEditStilus", getStilus(w->kerdezTextEdit));
    setParameter(&iniFajlSt, "valaszTextEditStilus", w->valaszTextEditStilus);
    setParameter(&iniFajlSt, "valaszTextEditStilusCh", w->valaszTextEditStilusCh);
    setParameter(&iniFajlSt, "valaszTextEditStilusPin", w->valaszTextEditStilusPin);
    setParameter(&iniFajlSt, "pipeTextEditStilus", getStilus(w->pipeTextEdit));
    setParameter(&iniFajlSt, "sugoBetuMeret", (QString::number(w->ui->sugoTextBrowser->font().pointSize())));
    setParameter(&iniFajlSt, QString::fromUtf8("forrásSrt"),
            (w->ui->bTextEdit3->toPlainText()));
    setParameter(&iniFajlSt, QString::fromUtf8("célSrt"),
            (w->ui->bTextEdit4->toPlainText()));
    QString st = QString::number(w->ui->splitter->sizes()[0]);
    st += "/" + QString::number(w->ui->splitter->sizes()[1]);
    setParameter(&iniFajlSt, "SplitterArany", st);
    for (int i=0; i < sz->szoList.count(); i++) {
        sz->szoList[i] = sz->szoList[i].replace("\n", QString::fromUtf8("÷1"));
    }
    setParameter(&iniFajlSt, "szoList", sz->szoList.join("|"));
    setParameter(&iniFajlSt, "Nyelv", w->nyelv);
    //qWarning("$foablak=" + iniSt.toUtf8());
    QStToFajl(iniFajlNev, iniFajlSt, false);
}

void kilepes(){
    iniKiir();
    pipeThreadVege = true;
}

LPTSTR QStringToLPTSTR(QString st) {
    return (LPTSTR)st.utf16();
}

QString LPTSTRToQString(LPTSTR st) {
    return QString::fromUtf16((const ushort *)st);
}

void setParameter(QString *forras, QString parNev, QString ertek) {
    QRegExp rx("\n\\$" + parNev + "=([^\\n#]*)[#\\n]");
    int pos = rx.indexIn(*forras);
    //qWarning("\nforras_1=" + forras->toUtf8());
    forras->replace(rx, "\n$" + parNev + "=" + ertek + "\n");
    //qWarning("\nforras_2=" + forras->toUtf8());
}



//QString getParameter(QString forras, QString parNev) {
//    QString eR = NULL;
//    QRegExp rx("\\n\\$" + parNev + "=([^\\n#]*)[#\\n]");
//    if (rx.indexIn("\n" + forras) != -1) {
//        eR = rx.cap(1);
//    }
//    eR = eR.split("#")[0];
//    eR = eR.trimmed();
//    return eR;
//}

QString getParameter(QString *forras, QString parNev, QString defaultErtek) {
    QString eR = "";
    QRegExp rx("\\n\\$" + parNev + "=([^\\n#]*)[\\n#]+");
    if (rx.indexIn("\n" + *forras) != -1) {
        eR = rx.cap(1);
    }
    eR = eR.split("#")[0];
    eR = eR.trimmed();
    if (defaultErtek != NULL && eR == "") {
        eR = defaultErtek;
        *forras = *forras + "\n#(default) " + parNev + ":\n$" + parNev + "=" + eR + "\n";
    }
    return eR;
}



QString fajlToQString(QString fajlNev) {
    //Beolvassa a fájlt QString-be Utf-8 kódolással, CR (carrige return) karaktereket kiveszi

    QFile fajl(fajlNev);
    if (!fajl.open(QIODevice::ReadOnly)) {
        qWarning("\nHIBA: fajlToQString, ez nincs meg: " + fajlNev.toUtf8());
        exit(233);
    }
    QByteArray ba = fajl.readAll();
    fajl.close();
    QString st = QString::fromUtf8(ba);

    st = st.trimmed();
    st = st + "\n";     //muindenképpen legyen a végén új sor jel
    st.replace("\r", "");    //kocsi vissza kiszedése
    //qWarning("fajlToQString, st = " + st.toUtf8());
    return st;
}

void QStToFajl(QString fajlNev, QString st, bool kellBOM) {
    //st-t kiírja Utf-8 kódolással

    QFile fajl(fajlNev);
    if (!fajl.open(QIODevice::WriteOnly | QIODevice::Text)) {
        qWarning(QString::fromUtf8("HIBA: QStringToFajl, ez nem írható ki: ").toUtf8() + fajlNev.toUtf8());
        exit(233);
    }
    QByteArray ba = st.toUtf8();
    if (kellBOM) {
        if (ba[0] != (char) 0xef || ba[0] != (char) 0xbb || ba[0] != (char) 0xbf) {
            ba.insert(0,   (char) 0xbf);
            ba.insert(0,   (char) 0xbb);
            ba.insert(0,   (char) 0xef);
        }
    }
    fajl.write(ba);
    fajl.close();
}

QString qRectToqString(QRect r) {
    QString st = QString::number(r.x());
    st = st + "," + QString::number(r.y());
    st = st + "," + QString::number(r.width());
    st = st + "," + QString::number(r.height());
    return st;
}

QRect qStringToqRect(QString st) {
    QStringList stL = st.split(",");
    QRect r(stL[0].toInt(), stL[1].toInt(), stL[2].toInt(), stL[3].toInt());
    return r;
}

QString getStilus(SaTextEdit *te) {
    QString st = te->font().family();
    st = st + "|" + QString::number(te->betuSzin.red());
    st = st + "|" + QString::number(te->betuSzin.green());
    st = st + "|" + QString::number(te->betuSzin.blue());
    QPalette pal = te->palette();
    st = st + "|" + QString::number(te->hatterSzin.red());
    st = st + "|" + QString::number(te->hatterSzin.green());
    st = st + "|" + QString::number(te->hatterSzin.blue());
    st = st + "|" + QString::number(te->font().pointSize());
    st = st + "|" + QString::number(te->font().bold());
    return st;
}

void setStilus(SaTextEdit *te, QString st) {
//    qWarning("setStilus: st = " + st.toLatin1() + ", objectName: " + te->objectName().toLatin1());
    te->hide();
    QStringList stL = st.split("|");
    QFont font = te->font();
    font.setFamily(stL[0]);

    //Ez a karakterek betűszíne:
    QColor col(stL[1].toInt(), stL[2].toInt(), stL[3].toInt());
    font.setPointSize(stL[7].toInt());
    font.setBold(stL[8].toInt());
    te->setFont(font);
    te->setBetuSzin(col);

    //Ez a karakterek háttérszíne:
    col = QColor(stL[4].toInt(), stL[5].toInt(), stL[6].toInt());
    te->setHattertSzin(col);
}

