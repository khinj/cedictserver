﻿CedictServer
============

[Magyar leírás itt.](md/readme_hun.md)

CedictServer is a tool on Windows 10 to learn Chinese based on the CEDICT Chinese-English dictionary.  
Download this dictionary from the [CEDICT website](http://www.mdbg.net/chindict/chindict.php?page=cedict) (cedict_1_0_ts_utf-8_mdbg.zip or cedict_1_0_ts_utf-8_mdbg.txt.gz), unzip, and on tab "Settings", in the text-box under "CEDICT dictionary file (e.g. cedict_ts.u8) location" define its location on your computer.
You can use your own dictionary file too, if the format of its rows:  
    Chinese word÷pinyin÷translation  
, also define its location.
You can select a tab with mouse click or using Ctrl + PageUp/PageDown keys. Font size in text-boxes can be adjusted with Ctrl + mouse-wheel. You can also set the font type, font color and background color of the text-boxes by right-clicking.

Japanese language can be also used. For this, download JMdict_e.gz file from the website of [JMDict Project](https://www.edrdg.org/jmdict/j_jmdict.html) (see the row "the current version with only the English translations"), unzip it, and define the location of the JMDict_e file on tab "Settings". Using this makes the start of CedictServer a couple of minutes longer.

**Functions:**

* Search:  
You can search using the functions available in the combo box (e.g. FvChToPin: Chines text -> pinyin text). If "Use own dictionary" is selected, and it contains the searched text, then there is no search in the CEDICT dictionary. While using FvChToPin function, uppercase pinyin result of CEDICT dictionary (property name) is shown only if "Uppercase pinyin" is selected.

* Settings  
At first start of the application you should define the location of the CEDICT dictionary file and the own dictionary file (the downloadable zip file contains demo files "cedict_ts_teszt.u8" and "localLista.txt").  
Using the bottom side of the tab you can write **pinyin text under Chinese text in a given \*.srt subtitle file**. The source file can be other kind of text file.

* HTML Request  
While CedictServer is running, functions of the combo box on the "Search" tab is available for other applications using the port 50000. _CS_TCP_IP.xls contained in this project is such an application. You can try this in a web browser:  
    http://localhost:50000/FvChToPin|%E4%BD%A0%E5%A5%BD  
(Response should be: nǐhǎo.)

* Pipe  
While CedictServer is running, functions of the combo box on the "Search" tab is available for other applications using pipe (communication channel of the operation system). [SaVLC media player](https://bitbucket.org/khinj/savlc/) is such an application.

The program can be downloaded from here: [CedictServer.zip](https://1drv.ms/f/s!AhEINfn0bmWKgQavfQq5bVxJpoRv). After unzipping run the CedictServer\release\CedictServer.exe program.

![Screenshot_1](md/CS_1.jpg "Screenshot_1")    ![Screenshot_2](md/CS_2.jpg "Screenshot_2")    ![Screenshot_3](md/CS_3.jpg "Screenshot_3")


**For developers:**

I used these "ingredients" for developing CedictServer:

* Windows 10 operation system (Win OS)

* Qt4.8

* Qt Creator 2.5.2

* MinGW (gcc (GCC) 4.8.1)

* Git


**Licence:**

My added part is completely free. Please see the licenses of the above mentioned "ingredients" on their web site. No warranty.
